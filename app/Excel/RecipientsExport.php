<?php 
namespace app\Excel;

use app\Models\mBlastChat;
use Illuminate\Contracts\View\View;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class RecipientsExport implements FromView, WithStyles, ShouldAutoSize
{
    private $id_blast_chat = 0;

    public function __construct($id_blast_chat){
        $this->id_blast_chat = $id_blast_chat;
    }

    public function view(): View
    {
        $data['blast_chat'] = mBlastChat::where('id_blast_chat', $this->id_blast_chat)->first();
        return view('blastChat.blastChatRecipientExcel', $data);
    }

    public function styles(Worksheet $sheet): array
    {
        return [
            'A' => ['numberFormat' => ['formatCode' => NumberFormat::FORMAT_TEXT]], 
            'B' => ['numberFormat' => ['formatCode' => NumberFormat::FORMAT_TEXT]], 
            'C' => ['numberFormat' => ['formatCode' => NumberFormat::FORMAT_TEXT]], 
            'D' => ['numberFormat' => ['formatCode' => NumberFormat::FORMAT_TEXT]], 
            'E' => ['numberFormat' => ['formatCode' => NumberFormat::FORMAT_TEXT]], 
            'F' => ['numberFormat' => ['formatCode' => NumberFormat::FORMAT_TEXT]], 
        ];
    }
}


