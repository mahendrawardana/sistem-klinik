<?php 

namespace app\Excel;

use app\Models\mBlastChat;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use app\Models\mBlastChatRecipient;
use Illuminate\Contracts\Session\Session;
use app\Helpers\Main;
use app\Models\mCallCode;
use DateTime;

class RecipientsImport implements ToCollection {
    private $blast_chat_id = 0; 
    public function __construct($blast_chat_id)
    {   
        $this->blast_chat_id = $blast_chat_id; 
    }
    public function collection(Collection $collections)
    {
        $blast_chat = mBlastChat::where('id_blast_chat', $this->blast_chat_id)->first();
        $template = $blast_chat->blc_chat_template;
        $variables = json_decode($blast_chat->blc_variable_key);
        $pairs = [];
        // pairing column in excel file with variable in database
        foreach($collections[0] as $i => $variable){
            foreach($variables as $variable_db){
                if ($variable_db == $variable){
                    $pairs[$i] = $variable;
                }
            }
        }

        foreach($collections as $i => $collection){
            if($i == 0 || !$collection[1]){
                continue;
            };
            $blast_chat_recipient = [];
            $chat_template = $template;
            $blast_chat_recipient['bcr_whatsapp_number'] = $collection[1];
            $var_array = [];
            foreach($collection as $k => $item) {

                foreach($pairs as $j => $pair) {
                    if($k == $j) {
                        $var_array[$pair] = $item;
                        $chat_template = str_replace("{".$pair."}","$item",$chat_template);
                    }
                }

                $blast_chat_recipient['bcr_message'] = $chat_template;
                $blast_chat_recipient['id_country_call_code'] = mCallCode::where('ccc_country_code','unknown')->first()->id_country_call_code;
                $blast_chat_recipient['bcr_send_status'] = 'not_yet';
                $blast_chat_recipient['bcr_send_time']  = $blast_chat->blc_send_time;
                $blast_chat_recipient['bcr_variable_value'] = json_encode($var_array);
                $blast_chat_recipient['id_blast_chat'] = $this->blast_chat_id;
                $blast_chat_recipient['bcr_recipient_type'] = 'new';
            }
            mBlastChatRecipient::create($blast_chat_recipient);
        }
    }
}


