<?php

namespace app\Http\Controllers\Appointment;

use app\Mail\appointmentNotification;
use app\Mail\reminderSchedule;
use app\Models\mAction;
use app\Models\mAppointment;
use app\Models\mCity;
use app\Models\mConsult;
use app\Models\mControl;
use app\Models\mKaryawan;
use app\Models\mPatient;
use app\Models\mProvince;
use app\Models\mReminderHistory;
use app\Models\mSubdistrict;
use app\Models\mUserRole;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use app\Http\Controllers\MasterData\Pekerjaan;
use app\Mail\reminderScheduleManual;
use app\Models\mCallCode;
use app\Models\mPekerjaan;
use app\Models\mReminderSetting;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use app\Rules\PhoneNumber;

class Appointment extends Controller
{
    private $breadcrumb;
    private $view = [
        'need_type' => ["search_field" => "appointment.need_type"],
        'name' => ["search_field" => "patient.name"],
        'weight' => ["search_field" => "patient.name"],
        'birthday' => ["search_field" => "patient.birthday"],
        'appointment_time' => ["search_field" => "appointment.appointment_time"],
        'phone_1' => ["search_field" => "patient.phone_1"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['appointment'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $name = $request->name;
        $date_from = $request->date_from ? $request->date_from : date('01-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("t-m-Y", strtotime($date_from));
        $province = mProvince::orderBy('province_name', 'ASC')->get();
        $pekerjaan = mPekerjaan::get()->map(function ($item) {
            $item->id_en = Main::encrypt($item->id_pekerjaan);
            return $item;
        });

        $datatable_column = [
            ["data" => "no"],
            ["data" => "need_type"],
            ["data" => "name"],
            ["data" => "weight"],
            ["data" => "age"],
            ["data" => "appointment_time"],
            ["data" => "location"],
            ["data" => "reminder_status"],
            ["data" => "options"],
        ];
        $call_codes = mCallCode::get();
        $data = array_merge($data, [
            'pekerjaan' => $pekerjaan,
            'province' => $province,
            'call_codes' => $call_codes,
            'name' => $name,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'datatable_column' => $datatable_column
        ]);


        return view('appointment/appointment/appointmentList', $data);
    }

    function data_table(Request $request)
    {

        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $name = $request->name;

        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $total_data = mAppointment
            ::leftJoin('patient', 'patient.id_patient', '=', 'appointment.id_patient')
            ->whereIn('appointment.status', ['prepare', 'send'])
            ->whereBetween('appointment.appointment_time', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->where('patient.name', 'LIKE', '%' . $name . '%')
            ->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_appointment'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mAppointment
            ::select([
                'appointment.*',
                'patient.*',
                'appointment.status AS appointment_status'
            ])
            ->leftJoin('patient', 'patient.id_patient', '=', 'appointment.id_patient')
            ->whereIn('appointment.status', ['prepare', 'send'])
            ->whereBetween('appointment.appointment_time', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->where('patient.name', 'LIKE', '%' . $name . '%')
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $location = '';
            if ($row->need_type == 'consult') {
                $location = mConsult::select('consult_location')->where('id_consult', $row->id_consult)->value('consult_location');
            } else if ($row->need_type == 'action') {
                $location = mAction::select('action_location')->where('id_action', $row->id_action)->value('action_location');
            } else if ($row->need_type == 'action_done') {
                $location = mAction::select('action_location')->where('id_action', $row->id_action)->value('action_location');
            } else if ($row->need_type == 'control') {
                $location = mControl::select('control_location')->where('id_control', $row->id_control)->value('control_location');
            }

            $nestedData['no'] = $no;
            $nestedData['need_type'] = Main::need_type_span($row->need_type, $row->control_step);
            $nestedData['name'] = $row->name;
            $nestedData['weight'] = $row->weight . ' Kg';
            $nestedData['age'] = Main::format_age($row->birthday);
            $nestedData['appointment_time'] = Main::format_datetime($row->appointment_time);
            $nestedData['location'] = Main::locationLabelView($location);
            $nestedData['reminder_status'] = $row->whatsapp_send == 'yes' ? Main::appointment_status($row->appointment_status) : '<span class="m-badge m-badge--default m-badge--wide">Tidak Direminder</span>';
            $nestedData['options'] = '

                    <div class="dropdown" data-id-appointment="' . $row->id_appointment . '">
                        <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                                type="button"
                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            Menu
                        </button>

                        <div class="dropdown-menu dropdown-menu-right"
                             aria-labelledby="dropdownMenuButton">
                            <a class="akses-detail dropdown-item btn-modal-general"
                                href="#"
                               data-route="' . route('appointmentDetail', ['id_appointment' => Main::encrypt($row->id_appointment)]) . '">
                                <i class="la la-info"></i>
                                Detail
                            </a>
                            <a class="akses-edit dropdown-item btn-modal-general"
                               href="#"
                               data-route="' . route('appointmentEdit', ['id_appointment' => Main::encrypt($row->id_appointment)]) . '">
                                <i class="la la-pencil"></i>
                                Edit
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="akses-delete dropdown-item btn-hapus"
                               data-route="' . route('appointmentDelete', ['id_appointment' => Main::encrypt($row->id_appointment)]) . '"
                               href="#">
                                <i class="la la-remove"></i>
                                Hapus
                            </a>
                        </div>
                    </div>
                ';


            $data[] = $nestedData;
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function detail($id_appointment = '')
    {
        $id_appointment = Main::decrypt($id_appointment);
        $row = mAppointment
            ::with([
                'patient',
                'patient.province',
                'patient.city',
                'patient.subdistrict',
                'patient.pekerjaan'
            ])
            ->where('id_appointment', $id_appointment)
            ->first();
        $data = [
            'row' => $row
        ];

        return view('appointment/appointment/appointmentDetail', $data);
    }

    function member_new()
    {
        $province = mProvince::orderBy('province_name', 'ASC')->get();
        $pekerjaan = mPekerjaan::get()->map(function ($item) {
            $item->id_en = Main::encrypt($item->id_pekerjaan);
            return $item;
        });
        $call_codes = mCallCode::get();
        $data = [
            'province' => $province,
            'pekerjaan' => $pekerjaan,
            'call_codes' => $call_codes
        ];
        return view('appointment/appointment/appointmentMemberNew', $data);
    }

    function member_new_insert(Request $request)
    {
        if ($request->from == 'admin') {
            $request->validate([
                'need_type' => 'required',
                'location' => 'required',
                'name' => 'required',
                'address' => 'required',
                'pekerjaan' => 'required',
                'birthday' => 'required|date_format:d-m-Y',
                'id_province' => 'required',
                'id_city' => 'required',
                'id_subdistrict' => 'required',
                'religion' => 'required',
                'weight' => 'required',
                'appointment_date' => 'required|date_format:d-m-Y|after:' . date('Y-m-d', strtotime('yesterday')),
                'appointment_hours' => 'required',
                'phone_1' => ['required'],
            ]);
        } else {
            $request->validate([
                'need_type' => 'required',
                'location' => 'required',
                'place_to_study' => 'required',
                'study' => 'required',
                'name' => 'required',
                'address' => 'required',
                'pekerjaan' => 'required',
                'birthday' => 'required|date_format:d-m-Y',
                'id_province' => 'required',
                'id_city' => 'required',
                'id_subdistrict' => 'required',
                'religion' => 'required',
                'weight' => 'required',
                'appointment_date' => 'required|date_format:d-m-Y|after:' . date('Y-m-d', strtotime('yesterday')),
                'appointment_hours' => 'required',
                'phone_1' => ['required'],
            ]);
        }

        $need_type = $request->need_type;
        $control_step = $request->control_step;
        $location = $request->location;
        $name = $request->name;
        $place_to_study = $request->place_to_study;
        $study = $request->study;
        $religion = $request->religion;
        $birthday = $request->birthday;
        $id_province = $request->id_province;
        $id_city = $request->id_city;
        $id_subdistrict = $request->id_subdistrict;
        $address = $request->address;
        $weight = $request->weight;
        $appointment_date = $request->appointment_date;
        $appointment_hours = $request->appointment_hours;
        $appointment_time = Main::format_date_db($appointment_date) . ' ' . Main::format_time_db($appointment_hours);
        $phone_1 = Main::fixNumberPhonePrefix($request->phone_1);
        $phone_1 = $request->call_code . $phone_1;
        $phone_2 = Main::fixNumberPhonePrefix($request->phone_2);
        $phone_2 = $request->call_code . $phone_2;
        $id_user = Session::has('user') ? Session::get('user')['id'] : '';
        $via = $request->via;
        $pekerjaan = $request->pekerjaan;
        $id_country_call_code = mCallCode::where('ccc_call_code', $request->call_code)->value('id_country_call_code');

        DB::beginTransaction();
        try {

            $data_patient = [
                'id_province' => $id_province,
                'id_city' => $id_city,
                'id_country_call_code' => $id_country_call_code,
                'id_pekerjaan' => $pekerjaan,
                'id_subdistrict' => $id_subdistrict,
                'id_user' => $id_user,
                'name' => $name,
                'study' => $study,
                'place_to_study' => $place_to_study,
                'religion' => $religion,
                'birthday' => Main::format_date_db($birthday),
                'weight' => $weight,
                'address' => $address,
                'phone_1' => $phone_1,
                'phone_2' => $phone_2,
                'status' => 'appointment',
            ];

            $id_patient = mPatient::create($data_patient)->id_patient;

            $data_appointment = [
                'id_patient' => $id_patient,
                'id_user' => $id_user,
                'need_type' => $need_type,
                'appointment_time' => $appointment_time,
                'via' => $via,
                'status' => 'prepare'
            ];

            if ($need_type == 'control') {
                $data_appointment['control_step'] = $control_step;
            }

            $id_appointment = mAppointment::create($data_appointment)->id_appointment;

            if ($need_type == 'consult') {
                $data_consult = [
                    'id_patient' => $id_patient,
                    'id_appointment' => $id_appointment,
                    'id_user' => $id_user,
                    'consult_location' => $location,
                    'consult_time' => Main::format_datetime_db($appointment_time),
                    'status' => 'process'
                ];
                $id_consult = mConsult::create($data_consult)->id_consult;

                mAppointment::where('id_appointment', $id_appointment)->update(['id_consult' => $id_consult]);
            } elseif ($need_type == 'action') {
                $data_action = [
                    'id_patient' => $id_patient,
                    'id_consult' => '',
                    'id_appointment' => $id_appointment,
                    'id_user' => $id_user,
                    'action_location' => $location,
                    'action_time' => Main::format_datetime_db($appointment_time),
                    'status' => 'process'
                ];

                $id_action = mAction::create($data_action)->id_action;

                mAppointment::where('id_appointment', $id_appointment)->update(['id_action' => $id_action]);
            } elseif ($need_type == 'control') {

                $data_control = [
                    'id_patient' => $id_patient,
                    'id_action' => '',
                    'id_appointment' => $id_appointment,
                    'id_user' => $id_user,
                    'control_location' => $location,
                    'control_time' => Main::format_datetime_db($appointment_time),
                    'status' => 'process',
                    'control_number' => $control_step
                ];

                $id_control = mControl::create($data_control)->id_control;

                mAppointment::where('id_appointment', $id_appointment)->update(['id_control' => $id_control]);
            }

            if ($via == 'user') {
//                Mail::to("sunatuntuksemua@gmail.com")->send(new appointmentNotification($id_appointment));
                //                Mail::to("mahendra.adi.wardana@gmail.com")->send(new appointmentNotification($id_appointment));

                $data_reminder_history = [
                    'status' => 'success',
                    'title' => 'New Appointment Notification',
                    'message' => 'Check Inbox Email sunatuntuksemua@gmail.com',
                    'email' => 'sunatuntuksemua@gmail.com',
                    'id_patient' => $id_patient,
                    'type' => 'reminder_new_appointment',
                    'id_appointment' => $id_appointment
                ];

                mReminderHistory::create($data_reminder_history);
            }
            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

        //        return Response::json(array(
        //            'code'      =>  404,
        //            'message'   =>  'test',
        //            'data' =>  $phone_1
        //        ), 404);


    }

    function edit($id_appointment)
    {
        $id_appointment = Main::decrypt($id_appointment);

        $edit = mAppointment::with('patient', 'patient.pekerjaan')->where('id_appointment', $id_appointment)->first();
        $province = mProvince::orderBy('province_name', 'ASC')->get();
        $city = mCity::where('id_province', $edit->patient->id_province)->orderBy('city_name', 'ASC')->get();
        $subdistrict = mSubdistrict::where('id_city', $edit->patient->id_city)->orderBy('subdistrict_name', 'ASC')->get();
        $pekerjaan = mPekerjaan::get()->map(function ($item) {
            $item->id_en = Main::encrypt($item->id_pekerjaan);
            return $item;
        });
        $location = '';
        if ($edit->need_type == 'consult') {
            $location = mConsult::where('id_consult', $edit->id_consult)->value('consult_location');
        } else if ($edit->need_type == 'action') {
            $location = mAction::where('id_action', $edit->id_action)->value('action_location');
        } else if ($edit->need_type == 'control') {
            $location = mControl::where('id_control', $edit->id_control)->value('control_location');
        }
        $call_codes = mCallCode::get();
        $call_code = mCallCode::where('id_country_call_code', $edit->patient->id_country_call_code)->first();
        $call_code_selected = isset($call_code['id_country_call_code']) ? $call_code['id_country_call_code'] : '87';
        $call_code_len = isset($call_code['id_country_call_code']) ? strlen((string)$call_code['ccc_call_code']) : '62';
        $edit->phone_number = substr($edit->patient->phone_1, $call_code_len);
        $data = [
            'edit' => $edit,
            'call_code_select' => $call_code_selected,
            'call_codes' => $call_codes,
            'province' => $province,
            'city' => $city,
            'subdistrict' => $subdistrict,
            'location' => $location,
            'pekerjaan' => $pekerjaan,
        ];

        return view('appointment.appointment.appointmentEdit', $data);
    }

    function update(Request $request, $id_appointment)
    {

        $request->validate([
            'need_type' => 'required',
            'name' => 'required',
            'religion' => 'required',
            'location' => 'required',
            'birthday' => 'required|date_format:d-m-Y',
            'id_province' => 'required',
            'id_city' => 'required',
            'id_subdistrict' => 'required',
            'pekerjaan' => 'required',
            'address' => 'required',
            'weight' => 'required',
            'appointment_date' => 'required',
            'appointment_hours' => 'required',
            'phone_1' => ['required'],
        ]);
        $id_country_call_code = mCallCode::where('ccc_call_code', $request->call_code)->value('id_country_call_code');
        $pekerjaan = $request->pekerjaan;
        $id_appointment = Main::decrypt($id_appointment);
        $study = $request->study;
        $place_to_study = $request->place_to_study;
        $id_patient = $request->id_patient;
        $need_type_new = $request->need_type;
        $control_step = $request->control_step;
        $location = $request->location;
        $name = $request->name;
        $religion = $request->religion;
        $birthday = $request->birthday;
        $id_province = $request->id_province;
        $id_city = $request->id_city;
        $id_subdistrict = $request->id_subdistrict;
        $address = $request->address;
        $weight = $request->weight;
        $appointment_date = $request->appointment_date;
        $appointment_hours = $request->appointment_hours;
        $appointment_time = Main::format_date_db($appointment_date) . ' ' . Main::format_time_db($appointment_hours);
        $phone_1 = Main::fixNumberPhonePrefix($request->phone_1);
        $phone_1 = $request->call_code . $phone_1;
        $phone_2 = $request->phone_2;
        $appointment = mAppointment::where('id_appointment', $id_appointment)->first();
        $need_type_old = $appointment->need_type;
        $id_user = Session::get('user')['id'];


        DB::beginTransaction();
        try {
            $data_patient = [
                'id_province' => $id_province,
                'id_city' => $id_city,
                'id_pekerjaan' => $pekerjaan,
                'id_country_call_code' => $id_country_call_code,
                'id_subdistrict' => $id_subdistrict,
                'study' => $study,
                'place_to_study' => $place_to_study,
                'name' => $name,
                'religion' => $religion,
                'birthday' => Main::format_date_db($birthday),
                'weight' => $weight,
                'address' => $address,
                'phone_1' => $phone_1,
                'phone_2' => $phone_2,
                'status' => $need_type_new,
            ];
            mPatient::where('id_patient', $id_patient)->update($data_patient);

            $data_appointment = [
                'id_patient' => $id_patient,
                'need_type' => $need_type_new,
                'appointment_time' => Main::format_datetime_db($appointment_time),
            ];

            if ($need_type_new == 'control') {
                $data_appointment['control_step'] = $control_step;
            }

            mAppointment::where('id_appointment', $id_appointment)->update($data_appointment);

            if ($need_type_old == 'consult' && $need_type_new == 'consult') {

                $check_consult_exist = mConsult::where('id_appointment', $id_appointment)->count();
                if ($check_consult_exist == 0) {
                    $data_consult = [
                        'id_patient' => $id_patient,
                        'id_appointment' => $id_appointment,
                        'id_user' => $id_user,
                        'consult_location' => $location,
                        'consult_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process'
                    ];
                    $id_consult = mConsult::create($data_consult)->id_consult;
                    mAppointment::where('id_appointment', $id_appointment)->update(['id_consult' => $id_consult]);
                } else {
                    $data_consult = [
                        'consult_time' => Main::format_datetime_db($appointment_time),
                        'consult_location' => $location,
                        'status' => 'process'
                    ];

                    mConsult::where('id_consult', $appointment->id_consult)->update($data_consult);
                }
            } elseif ($need_type_old == 'action' && $need_type_new == 'action') {

                $check_action_exist = mAction::where('id_appointment', $id_appointment)->count();
                if ($check_action_exist == 0) {
                    $data_action = [
                        'id_patient' => $id_patient,
                        'id_consult' => $appointment->id_consult,
                        'id_appointment' => $id_appointment,
                        'id_user' => $id_user,
                        'action_location' => $location,
                        'action_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process'
                    ];
                    $id_action = mAction::create($data_action)->id_action;
                    mAppointment::where('id_appointment', $id_appointment)->update(['id_action' => $id_action]);
                } else {

                    $data_action = [
                        'action_time' => Main::format_datetime_db($appointment_time),
                        'action_location' => $location,
                        'status' => 'process'
                    ];

                    mAction::where('id_action', $appointment->id_action)->update($data_action);
                }
            } elseif ($need_type_old == 'control' && $need_type_new == 'control') {

                $check_control_exist = mControl::where('id_appointment', $id_appointment)->count();
                if ($check_control_exist == 0) {
                    $data_control = [
                        'id_appointment' => $id_appointment,
                        'id_patient' => $id_patient,
                        'id_action' => $appointment->id_action,
                        'id_user' => $id_user,
                        'control_location' => $location,
                        'control_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process',
                        'control_number' => $control_step,
                    ];
                    $id_control = mControl::create($data_control)->id_control;
                    mAppointment::where('id_appointment', $id_appointment)->update(['id_control' => $id_control]);
                } else {
                    $data_control = [
                        'control_time' => Main::format_datetime_db($appointment_time),
                        'control_location' => $location,
                        'status' => 'process'
                    ];

                    mControl::where('id_control', $appointment->id_control)->update($data_control);
                }
            } elseif ($need_type_old == 'consult' && $need_type_new == 'action') {
                $id_consult = $appointment->id_consult;
                $check_appointment_action_exist = mAction::where('id_appointment', $id_appointment)->count();

                if ($check_appointment_action_exist == 0) {
                    $data_action = [
                        'id_patient' => $id_patient,
                        'id_consult' => '',
                        'id_appointment' => $id_appointment,
                        'id_user' => $id_user,
                        'action_location' => $location,
                        'action_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process',
                    ];

                    $id_action = mAction::create($data_action)->id_action;
                } else {
                    $id_action = mAction::where('id_appointment', $id_appointment)->value('id_action');
                    $data_action = [
                        'action_time' => Main::format_datetime_db($appointment_time),
                        'action_location' => $location,
                        'status' => 'process'
                    ];
                    mAction::where('id_action', $id_action)->update($data_action);
                }
                mAppointment
                    ::where('id_appointment', $id_appointment)
                    ->update(['id_action' => $id_action]);

                mConsult
                    ::where('id_consult', $id_consult)
                    ->update([
                        'status' => 'cancel',
                        'cancel_time' => date('Y-m-d H:i:s')
                    ]);
            } elseif ($need_type_old == 'consult' && $need_type_new == 'control') {
                $id_consult = $appointment->id_consult;
                $check_appointment_control_exist = mControl::where('id_appointment', $id_appointment)->count();

                if ($check_appointment_control_exist == 0) {
                    $data_control = [
                        'id_appointment' => $id_appointment,
                        'id_patient' => $id_patient,
                        'id_action' => '',
                        'id_user' => $id_user,
                        'control_location' => $location,
                        'control_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process',
                        'control_number' => $control_step,
                    ];

                    $id_control = mControl::create($data_control)->id_control;
                } else {
                    $id_control = mControl::where('id_appointment', $id_appointment)->value('id_control');
                    $data_control = [
                        'control_time' => Main::format_datetime_db($appointment_time),
                        'control_location' => $location,
                        'control_number' => $control_step,
                        'status' => 'process'
                    ];
                    mControl::where('id_control', $id_control)->update($data_control);
                }
                mAppointment
                    ::where('id_appointment', $id_appointment)
                    ->update(['id_control' => $id_control, 'control_step' => $control_step]);

                mConsult
                    ::where('id_consult', $id_consult)
                    ->update([
                        'status' => 'cancel',
                        'cancel_time' => date('Y-m-d H:i:s')
                    ]);
            } elseif ($need_type_old == 'action' && $need_type_new == 'consult') {
                $id_action = $appointment->id_action;
                $check_appointment_consult_exist = mConsult::where('id_appointment', $id_appointment)->count();

                if ($check_appointment_consult_exist == 0) {
                    $data_consult = [
                        'id_patient' => $id_patient,
                        'id_appointment' => $id_appointment,
                        'id_user' => $id_user,
                        'consult_location' => $location,
                        'consult_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process',
                    ];

                    $id_consult = mConsult::create($data_consult)->id_consult;
                } else {
                    $id_consult = mConsult::where('id_appointment', $id_appointment)->value('id_consult');
                    $data_consult = [
                        'consult_time' => Main::format_datetime_db($appointment_time),
                        'consult_location' => $location,
                        'status' => 'process'
                    ];
                    mConsult::where('id_consult', $id_consult)->update($data_consult);
                }
                mAppointment
                    ::where('id_appointment', $id_appointment)
                    ->update(['id_consult' => $id_consult]);

                mAction
                    ::where('id_action', $id_action)
                    ->update([
                        'status' => 'cancel',
                        'cancel_time' => date('Y-m-d H:i:s')
                    ]);
            } elseif ($need_type_old == 'action' && $need_type_new == 'control') {
                $id_action = $appointment->id_action;
                $check_appointment_control_exist = mControl::where('id_appointment', $id_appointment)->count();

                if ($check_appointment_control_exist == 0) {
                    $data_control = [
                        'id_patient' => $id_patient,
                        'id_appointment' => $id_appointment,
                        'id_user' => $id_user,
                        'id_action' => '',
                        'control_location' => $location,
                        'control_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process',
                        'control_number' => $control_step,
                    ];

                    $id_control = mControl::create($data_control)->id_control;
                } else {
                    $id_control = mControl::where('id_appointment', $id_appointment)->value('id_control');
                    $data_control = [
                        'control_time' => Main::format_datetime_db($appointment_time),
                        'control_location' => $location,
                        'control_number' => $control_step,
                        'status' => 'process'
                    ];
                    mControl::where('id_control', $id_control)->update($data_control);
                }
                mAppointment
                    ::where('id_appointment', $id_appointment)
                    ->update(['id_control' => $id_control, 'control_step' => $control_step]);

                mAction
                    ::where('id_action', $id_action)
                    ->update([
                        'status' => 'cancel',
                        'cancel_time' => date('Y-m-d H:i:s')
                    ]);
            } elseif ($need_type_old == 'control' && $need_type_new == 'consult') {
                $id_control = $appointment->id_control;
                $check_appointment_consult_exist = mConsult::where('id_appointment', $id_appointment)->count();

                if ($check_appointment_consult_exist == 0) {
                    $data_consult = [
                        'id_patient' => $id_patient,
                        'id_appointment' => $id_appointment,
                        'id_user' => $id_user,
                        'control_location' => $location,
                        'control_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process',
                    ];

                    $id_consult = mConsult::create($data_consult)->id_consult;
                } else {
                    $id_consult = mConsult::where('id_appointment', $id_appointment)->value('id_consult');
                    $data_consult = [
                        'consult_time' => Main::format_datetime_db($appointment_time),
                        'consult_location' => $location,
                        'status' => 'process'
                    ];
                    mConsult::where('id_consult', $id_consult)->update($data_consult);
                }
                mAppointment
                    ::where('id_appointment', $id_appointment)
                    ->update(['id_consult' => $id_consult]);

                mControl
                    ::where('id_control', $id_control)
                    ->update([
                        'status' => 'cancel',
                        'cancel_time' => date('Y-m-d H:i:s')
                    ]);
            } elseif ($need_type_old == 'control' && $need_type_new == 'action') {
                $id_control = $appointment->id_control;
                $check_appointment_action_exist = mAction::where('id_appointment', $id_appointment)->count();

                if ($check_appointment_action_exist == 0) {
                    $data_action = [
                        'id_patient' => $id_patient,
                        'id_consult' => '',
                        'id_appointment' => $id_appointment,
                        'id_user' => $id_user,
                        'action_location' => $location,
                        'action_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process',
                    ];

                    $id_action = mAction::create($data_action)->id_action;
                } else {
                    $id_action = mAction::where('id_appointment', $id_appointment)->value('id_action');
                    $data_action = [
                        'action_time' => Main::format_datetime_db($appointment_time),
                        'action_location' => $location,
                        'status' => 'process'
                    ];
                    mAction::where('id_action', $id_action)->update($data_action);
                }
                mAppointment
                    ::where('id_appointment', $id_appointment)
                    ->update(['id_action' => $id_action]);

                mControl
                    ::where('id_control', $id_control)
                    ->update([
                        'status' => 'cancel',
                        'cancel_time' => date('Y-m-d H:i:s')
                    ]);
            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function delete($id_appointment)
    {

        DB::beginTransaction();
        try {
            $id_appointment = Main::decrypt($id_appointment);
            $appointment = mAppointment::where('id_appointment', $id_appointment)->first();
            mAppointment::where('id_appointment', $id_appointment)->delete();
            mPatient::where('id_patient', $appointment->id_patient)->delete();

            if ($appointment->need_type == 'control') {
                mControl::where('id_control', $appointment->id_control)->delete();
            }

            if ($appointment->need_type == 'action') {
                mAction::where('id_action', $appointment->id_action)->delete();
            }

            if ($appointment->need_type == 'consult') {
                mConsult::where('id_consult', $appointment->id_consult)->delete();
            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function download(Request $request, $type)
    {

        if ($type != 'pdf' && $type != 'xls') {

            return;
        };

        $from = Main::format_date_db($request->from);
        $to = Main::format_date_db($request->to);

        $data_list = mAppointment::select([
            'appointment.*',
            'patient.*',
            'appointment.status AS appointment_status'
        ])
            ->leftJoin('patient', 'patient.id_patient', '=', 'appointment.id_patient')
            ->whereIn('appointment.status', ['prepare', 'send'])
            ->whereBetween('appointment.appointment_time', [$from . " 00:00:00", $to . " 23:59:59"])
            ->get()->map(function ($row) {

                $location = '';
                if ($row->need_type == 'consult') {
                    $location = mConsult::select('consult_location')->where('id_consult', $row->id_consult)->value('consult_location');
                } else if ($row->need_type == 'action') {
                    $location = mAction::select('action_location')->where('id_action', $row->id_action)->value('action_location');
                } else if ($row->need_type == 'action_done') {
                    $location = mAction::select('action_location')->where('id_action', $row->id_action)->value('action_location');
                } else if ($row->need_type == 'control') {
                    $location = mControl::select('control_location')->where('id_control', $row->id_control)->value('control_location');
                }
                $row->location = $location;
                return $row;
            });

        $data = [
            'appointment' => $data_list,
            'date_from' => $request->from,
            'date_to' => $request->to
        ];
        return $type == 'pdf' ?
            view('appointment.appointment.download.downloadPdf', $data) :
            view('appointment.appointment.download.downloadXls', $data);
    }

    function schedule(Request $request)
    {
        $from = null;
        $to = null;
        if ($request->from && $request->to) {
            $from = Main::format_date_db($request->from);
            $to = Main::format_date_db($request->to);
        }
        // consult
        Mail::to("sunatuntuksemua@gmail.com")->send(new reminderScheduleManual('consult', $from, $to));
        // action
        Mail::to("sunatuntuksemua@gmail.com")->send(new reminderScheduleManual('action', $from, $to));
        // control
        Mail::to("sunatuntuksemua@gmail.com")->send(new reminderScheduleManual('control', $from, $to));

        $data_reminder_history = [
            'status' => 'success',
            'title' => 'Reminder Jadwal',
            'type' => 'reminder_schedule',
            'message' => 'Check Inbox Email sunatuntuksemua@gmail.com',
            'email' => 'sunatuntuksemua@gmail.com'
        ];
        mReminderHistory::create($data_reminder_history);
    }
}
