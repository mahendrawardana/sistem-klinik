<?php

namespace app\Http\Controllers\BlastChat;

use app\Excel\RecipientsImport;
use app\Excel\RecipientsExport;
use app\Helpers\Main;
use app\Http\Controllers\Controller;
use app\Models\mKaryawan;
use app\Models\mPatient;
use app\Models\mUser;
use app\Models\mUserRole;
use app\Rules\UsernameChecker;
use app\Rules\UsernameCheckerUpdate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use app\Rules\validateVariableName;
use app\Models\mBlastChat;
use app\Models\mBlastChatRecipient;
use app\Models\mCallCode;
use app\Rules\PhoneNumber;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class BlastChat extends Controller
{
  private $breadcrumb;

  function __construct()
  {
    $cons = Config::get('constants.topMenu');
    $this->breadcrumb = [
      [
        'label' => $cons['blast_chat'],
        'route' => ''
      ]
    ];
  }

  function index(Request $request)
  {
    $data = Main::data($this->breadcrumb);
    $keyword = $request->keyword;
    $date_from = $request->date_from ?? date('01-m-Y H:i');
    $date_to = $request->date_to ?? date("t-m-Y H:i", strtotime($date_from));
    $data = array_merge($data, [
      'date_from' => $date_from,
      'date_to' => $date_to,
      'keyword' => $keyword,
      'datatable_column' => [
        ['data' => 'no'],
        ['data' => 'judul'],
        ['data' => 'waktu_kirim'],
        ['data' => 'total_penerima'],
        ['data' => 'total_terkirim'],
        ['data' => 'total_gagal'],
        ['data' => 'waktu_pembuatan'],
        ['data' => 'menu']
      ]
    ]);

    return view('blastChat.blastChatList', $data);
  }

  function patient_list(Request $request)
  {
    $keyword = $request->keyword;
    $date_from = $request->date_from ? $request->date_from : date('01-01-2019 H:i:s');
    $date_to = $request->date_to ? $request->date_to : date("d-m-Y H:i:s");
    
    $datatable_column = [
      ["data" => "no"],
      ["data" => "name"],
      ["data" => "weight"],
      ["data" => "age"],
      ["data" => "address"],
      ["data" => "phone_1"],
      ["data" => "patient_status"],
      ["data" => "options"],
    ];

    $data = array(
      'datatable_column' => $datatable_column,
      'keyword' => $keyword,
      'date_from' => $date_from,
      'date_to' => $date_to,
    );

    return view('blastChat.patient.patientList', $data);
  }

  function insert(Request $request)
  {
    $request->validate([
      'nama_blast_chat' => 'required',
      'waktu_kirim' => 'required',
      'template' => 'required',
      'variable' => new validateVariableName()
    ]);

    // remove empty variable value 
    $request->variable = Main::array_filter_auto_index($request->variable, function ($value) {
      return $value !== null;
    });

    $datetime_arr = explode(' ', $request->waktu_kirim);
    $date = Main::format_date_db($datetime_arr[0]);
    $time = Main::format_time_db($datetime_arr[1]);
    $datetime = $date . ' ' . $time;
    $blast_chat = [
      'blc_title' => $request->nama_blast_chat,
      'blc_notes' => $request->catatan,
      'blc_variable_key' => json_encode($request->variable),
      'blc_chat_template' => $request->template,
      'blc_send_time' => $datetime,
      'blc_run_status' => 'not_yet',
    ];

    mBlastChat::create($blast_chat);
  }


  function data_table(Request $request)
  {

    $date_from = $request->date_from;
    $date_to = $request->date_to;
    $keyword = $request->keyword;
    $date_from_db = date('Y-m-d H:i:s', strtotime($date_from));
    $date_to_db = date('Y-m-d H:i:s', strtotime($date_to));

    $total_data = mBlastChat
      // ::leftJoin('patient', 'patient.id_patient', '=', 'consult.id_patient')
      //->whereIn('consult.status', ['process'])
      ::whereBetween('blc_send_time', [$date_from_db, $date_to_db])
      ->where('blc_title', 'LIKE', '%' . $keyword . '%')
      ->orWhere('blc_notes', 'LIKE', '%' . $keyword . '%')
      ->count();
    $limit = $request->input('length');
    $start = $request->input('start');
    $order_column = 'id_blast_chat'; //$columns[$request->input('order.0.column')];
    $order_type = $request->input('order.0.dir');

    $data_list = mBlastChat
      ::with(
        'blast_chat_recipient'
      )
      /*
            ->leftJoin('patient', 'patient.id_patient', '=', 'consult.id_patient')
            ->whereIn('consult.status', ['process'])
            */
      ->whereBetween('blc_send_time', [$date_from_db, $date_to_db])
      ->where('blc_title', 'LIKE', '%' . $keyword . '%')
      ->orWhere('blc_notes', 'LIKE', '%' . $keyword . '%')
      ->offset($start)
      ->limit($limit)
      ->orderBy($order_column, $order_type)
      ->get();

    $total_data++;
    // dd([$date_from_db.' > '.$date_to_db,$data_list]); // ["2023-03-16 03:15:00 > 2023-03-16 02:10:00", $data_list]
    $data = array();
    foreach ($data_list as $key => $row) {
      $key++;

      if ($order_type == 'asc') {
        $no = $key + $start;
      } else {
        $no = $total_data - $key - $start;
      }

      $nestedData['no'] = $no;
      $nestedData['judul'] = $row->blc_title;
      $nestedData['waktu_kirim'] = Main::format_datetime($row->blc_send_time);
      $nestedData['total_penerima'] = $row->blast_chat_recipient ? $row->blast_chat_recipient->count() : 0;
      $nestedData['total_terkirim'] = mBlastChatRecipient::where('id_blast_chat', $row->id_blast_chat)->where('bcr_send_status', 'sent')->count();
      $nestedData['total_gagal'] = mBlastChatRecipient::where('id_blast_chat', $row->id_blast_chat)->where('bcr_send_status', 'stop')->count();
      $nestedData['waktu_pembuatan'] = Main::format_datetime($row->created_at);
      $sending = '';
      $editable = $row->blc_run_status == 'run' ? '' : '
                             <a class="akses-edit dropdown-item btn-modal-general"
                               href="#"
                               data-route="' . route('blastChatEditModal', ['id_blast_chat' => Main::encrypt($row->id_blast_chat)]) . '">
                                <i class="la la-pencil"></i>
                                Edit
                            </a>';
      if ($row->blc_run_status == 'not_yet') {
        $sending =  '<span class="dropdown-item disabled"
                        data-route="' . "" . '"
                        href="#">
                        <i class="la la-clock-o"></i>
                        Menunggu dikirim </span>';
      } else if ($row->blc_run_status == 'run') {
        $sending = '<a class="akses-consult_done dropdown-item text-danger"
                        href="' . route('blastChatStop', ['id_blast_chat' => Main::encrypt($row->id_blast_chat)]) . '">
                        <i class="la la-plane" style="transform: rotate(-180deg);"></i>
                        Stop Kirim Semua Chat </a>';
      } else if ($row->blc_run_status == 'stop') {
        //TODO : add more 1 hour if admin wanna continue send this 
        $sending = '<a class="akses-consult_done dropdown-item"
                        href="' . route('blastChatSendAgain', ['id_blast_chat' => Main::encrypt($row->id_blast_chat)]) . '"
                        >
                        <i class="la la-plane"></i>
                        Lanjutkan kirim semua chat </a>';
      }

      $nestedData['menu'] = '
                      <div class="dropdown">
                        <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill" type="button"
                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            Menu
                        </button>
                        <div class="dropdown-menu dropdown-menu-right"
                             aria-labelledby="dropdownMenuButton">' . $sending . '
                            <a class="dropdown-item"
                               href="' . route('blastChatRecipientList', ['id_blast_chat' => Main::encrypt($row->id_blast_chat)]) . '">
                                <i class="la la-users"></i>
                                Daftar Penerima
                            </a>
                            <div class="dropdown-divider"></div>' . $editable . '
                            <a class="akses-detail dropdown-item btn-modal-general"
                               href="#"
                                data-route="' . route('blastChatDetailModal', ['id_blast_chat' => Main::encrypt($row->id_blast_chat)]) . '">
                                <i class="la la-info"></i>
                                Detail
                            </a>

                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item btn-hapus text-danger"
                               href="#"
                                data-route="' . route('blastChatDelete', ['id_blast_chat' => Main::encrypt($row->id_blast_chat)]) . '">
                                <i class="la la-times"></i>
                                Delete
                            </a>

                        </div>
                    </div>

                                   ';


      $data[] = $nestedData;
    }

    $json_data = array(
      "draw" => intval($request->input('draw')),
      "recordsTotal" => intval($total_data - 1),
      "recordsFiltered" => intval($total_data - 1),
      "data" => $data,
      'all_request' => $request->all()
    );

    return $json_data;
  }

  function modal_detail($id_blast_chat)
  {
    $id_blast_chat = Main::decrypt($id_blast_chat);
    $blast_chat = mBlastChat::with('blast_chat_recipient')->where('id_blast_chat', $id_blast_chat)->first();
    $blast_chat->blc_send_time = Main::format_datetime($blast_chat->blc_send_time);
    $data = [
      'blast_chat' => $blast_chat,
      'total_recepient' => $blast_chat->blast_chat_recipient->count(), 
      'total_send' => $blast_chat->blast_chat_recipient->where('bcr_send_status','sent')->count(), 
      'total_fail' => $blast_chat->blast_chat_recipient->where('bcr_send_status','stop')->count()
    ];
    return view('blastChat.blastChatDetailModal', $data);
  }

  function edit_modal($id_blast_chat)
  {
    $id_blast_chat = Main::decrypt($id_blast_chat);
    $blast_chat = mBlastChat::where('id_blast_chat', $id_blast_chat)->first();
    $variables = json_decode($blast_chat->blc_variable_key);
    $data = [
      'blast_chat' => $blast_chat,
      'variables' =>  array_slice($variables, 1),
      'first_variable' => $variables[0] ?? '',
      'id_blast_chat' => Main::encrypt($blast_chat->id_blast_chat)
    ];

    return view('blastChat.blastChatEditModal', $data);
  }

  function delete($id_blast_chat)
  {
    $id_blast_chat = Main::decrypt($id_blast_chat);
    mBlastChat::where('id_blast_chat', $id_blast_chat)->delete();
  }

  function stop($id_blast_chat)
  {
    $id_blast_chat = Main::decrypt($id_blast_chat);
    mBlastChat::where('id_blast_chat', $id_blast_chat)->update(['blc_run_status' => 'stop']);
    return redirect()->back();
  }

  function send_again($id_blast_chat)
  {
    $id_blast_chat = Main::decrypt($id_blast_chat);
    $blast_chat_time = strtotime(mBlastChat::where('id_blast_chat', $id_blast_chat)->value('blc_send_time'));
    $db_time = date('Y-m-d H:i:s', strtotime('+ 60 minutes')); // add 60 minutes from now
    $now = strtotime('+0 minutes');
    if ($blast_chat_time < $now) {
      mBlastChat::where('id_blast_chat', $id_blast_chat)->update(['blc_run_status' => 'run', 'blc_send_time' => $db_time]);
    } else {
      mBlastChat::where('id_blast_chat', $id_blast_chat)->update(['blc_run_status' => 'run']);
    }
    return redirect()->back();
  }
  function update(Request $request, $id_blast_chat)
  {
    $id_blast_chat = Main::decrypt($id_blast_chat);
    $request->validate([
      'nama_blast_chat' => 'required',
      'waktu_kirim' => 'required',
      'template' => 'required',
      'variable' => new validateVariableName()
    ]);

    // remove empty variable value 
    $request->variable = Main::array_filter_auto_index($request->variable, function ($value) {
      return $value !== null;
    });

    $blast_chat_old = mBlastChat::where('id_blast_chat', $id_blast_chat)->first();
    $datetime_arr = explode(' ', $request->waktu_kirim);
    $date = Main::format_date_db($datetime_arr[0]);
    $time = Main::format_time_db($datetime_arr[1]);
    $datetime = $date . ' ' . $time;
    $blast_chat = [
      'blc_title' => $request->nama_blast_chat,
      'blc_notes' => $request->catatan,
      'blc_variable_key' => json_encode($request->variable),
      'blc_chat_template' => $request->template,
      'blc_send_time' => $datetime,
    ];

    // update the send time in all recipient 
    if (strtotime($datetime) != strtotime($blast_chat_old->blc_send_time)) {
      mBlastChatRecipient::where('id_blast_chat', $id_blast_chat)
        ->where('bcr_send_status', 'not_yet')
        ->update(['bcr_send_time' => $datetime]);
    }

    mBlastChat::where(['id_blast_chat' => $id_blast_chat])
      ->update($blast_chat);
  }

  function recipient_modal_detail($id_recipient)
  {
    $id_recipient = Main::decrypt($id_recipient);
    $recipient = mBlastChatRecipient::with('patient')->where('id_blast_chat_recipient', $id_recipient)->first();
    $blast_chat = mBlastChat::where('id_blast_chat', $recipient->id_blast_chat)->first();
    $data = ['recipient' => $recipient, 'variables_key' => json_decode($blast_chat->blc_variable_key)];
    return view('blastChat.recipient.recipientModalDetail', $data);
  }

  function recipient_data_table(Request $request)
  {
    $id_blast_chat = Main::decrypt($request->id_blast_chat);
    $total_data = mBlastChat
      ::with('blast_chat_recipient')
      ->where('id_blast_chat', $id_blast_chat)
      ->first();

    $total_data = count($total_data->blast_chat_recipient->toArray());

    $limit = $request->input('length');
    $start = $request->input('start');
    $order_column = 'id_blast_chat_recipient';
    $order_type = $request->input('order.0.dir');

    $blast_chat = mBlastChat
      ::with(
        [
          'blast_chat_recipient' => function ($query) use ($order_column, $order_type, $start, $limit) {
            $query->orderBy($order_column, $order_type)->offset($start)->limit($limit);
          }
        ]
      )
      ->where('id_blast_chat', $id_blast_chat)
      ->first();
    $data_list = $total_data == 0 ? [] : $blast_chat->blast_chat_recipient;

    $total_data++;

    $data = array();
    foreach ($data_list as $key => $row) {
      $key++;

      if ($order_type == 'asc') {
        $no = $key + $start;
      } else {
        $no = $total_data - $key - $start;
      }

      $variables = json_decode($row->bcr_variable_value) ?? [];
      $variable_content = '<table class="table">';
      foreach ($variables as $key => $variable) {
        $value = $variable ?? ' - ';
        $variable_content .= '<tr>
          <td width="24px"><span class="" style="border-radius:4px; width: max-content"> '.$key.' </span></td>
          <td><span class="px-2 mx-1" style="border-radius:4px; width: max-content">: '.$value.' </span></td>
        </tr>';
        
      }

      $nestedData['no'] = $no;
      $nestedData['nomor_whatsapp'] = $row->bcr_whatsapp_number;
      $nestedData['waktu_kirim'] = Main::format_datetime($row->bcr_send_time);
      
      $nestedData['message'] = '<textarea class="text-dark" readonly style="background-color: transparent;border: none; resize: none;outline: none" rows="'.(count(explode(PHP_EOL,$row->bcr_message)) + 2).'">'.$row->bcr_message.'</textarea>';
      $nestedData['variable_content'] = $variable_content.'</table>';
      $nestedData['status_kirim'] = $row->bcr_send_status == 'not_yet'
        ? "Belum Terkirim"  : ($row->bcr_send_status == 'stop'
          ? "DiBerhentikan" : ($row->bcr_send_status == 'pending'
            ? 'Sedang Dikirim' : "Sudah terkirim"));
      $nestedData['jenis_penerima'] = $row->bcr_recipient_type == 'new' ? "Nomor Baru" : "Pasien Klinik";
      $editable = $row->bcr_send_status == 'not_yet' ?
        '<a class="akses-edit dropdown-item"
                                onclick="btn_modal_edit(this)"
                                href="#"
                                data-route="' . route('blastChatRecipientEditModal', ['id_recipient' => Main::encrypt($row->id_blast_chat_recipient)]) . '">
                                <i class="la la-pencil"></i>
                                Edit
                            </a>' : '';
      $nestedData['menu'] = '
                      <div class="dropdown">
                        <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill" type="button"
                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            Menu
                        </button>
                        <div class="dropdown-menu dropdown-menu-right"
                             aria-labelledby="dropdownMenuButton">' . $editable . '
                            <a class="akses-detail dropdown-item btn-modal-general"
                               href="#"
                                data-route="' . route('recipientModalDetail', ['id_recipient' => Main::encrypt($row->id_blast_chat_recipient)]) . '">
                                <i class="la la-info"></i>
                                Detail
                            </a>

                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item btn-hapus text-danger"
                               href="#"
                                data-route="' . route('blastChatRecipientDelete', ['id_blast_chat_recipient' => Main::encrypt($row->id_blast_chat_recipient)]) . '">
                                <i class="la la-times"></i>
                                Delete
                            </a>

                        </div>
                    </div>

                                   ';


      $data[] = $nestedData;
    }

    $json_data = array(
      "draw" => intval($request->input('draw')),
      "recordsTotal" => intval($total_data - 1),
      "recordsFiltered" => intval($total_data - 1),
      "data" => $data,
      'all_request' => $request->all()
    );

    return $json_data;
  }

  function recipient_edit($id_recipient_raw)
  {
    $call_codes = mCallCode::get()->map(function($item){
      $item->id_encrypt = Main::encrypt($item->id_country_call_code);
      return $item; 
    });
    $id_recipient = Main::decrypt($id_recipient_raw);
    $recipient = mBlastChatRecipient::where('id_blast_chat_recipient', $id_recipient)->with('blast_chat')->first();
    $call_code = $call_codes->where('id_country_call_code',$recipient->id_country_call_code)->first();
    if($recipient->id_country_call_code){      
      $call_code_len = strlen((string)$call_code->ccc_call_code);
      $recipient->bcr_whatsapp_number = substr($recipient->bcr_whatsapp_number,$call_code_len);
    }

    $data = [
      'call_codes' => $call_codes,
      'call_code_select' => $call_code,
      'recipient' => $recipient,
      'blast_chat' => $recipient->blast_chat,
      'send_time' => Main::format_datetime($recipient->bcr_send_time),
      'id_recipient' => $id_recipient_raw,
      'variables' => $recipient ?  json_decode($recipient->bcr_variable_value) : []
    ];

    return view('blastChat.recipient.recipientEditModal', $data);
  }

  function recipient_update(Request $request, $id_recipient)
  {
    $id_recipient = Main::decrypt($id_recipient);
    $id_patient = $request->id_patient ? Main::decrypt($request->id_patient) : null;
    $id_country_call_code = $id_patient ? mPatient::where('id_patient', $id_patient)->first()->id_country_call_code  : ( $request->call_code ?  Main::decrypt($request->call_code) : mCallCode::where('ccc_country_code','unknown')->first()->id_country_call_code);
    // validate 
    $request->validate([
      'nomor_whatsapp' => ['required', new PhoneNumber]
    ]);
    $recipient = [];
    $call_code = 62;
    if($id_country_call_code) {
      $recipient['id_country_call_code'] = $id_country_call_code ?? mCallCode::where('ccc_call_code', $call_code)->first()->id_country_call_code;
      $country_call_code = mCallCode::where('id_country_call_code', $id_country_call_code)->first();
      $call_code = $country_call_code->ccc_call_code;
    }
    
    $recipient = array_merge($recipient,[  
      'bcr_whatsapp_number' => $id_patient ? $request->nomor_whatsapp : $call_code. $request->nomor_whatsapp ,
      'id_patient' => $id_patient,
      'bcr_variable_value' => json_encode($request->variable_content),
      'bcr_send_status' => 'not_yet',
      'bcr_message' => $request->hasil_chat,
      'bcr_recipient_type' => $request->id_patient ? 'patient_exist' : 'new'
    ]);
    
    mBlastChatRecipient::where('id_blast_chat_recipient', $id_recipient)->update($recipient);
  }
  function generate_excel_file($id_blast_chat){
    $id_blast_chat = Main::decrypt($id_blast_chat);
    $blast_chat = mBlastChat::where('id_blast_chat', $id_blast_chat)->first();
    return Excel::download(new RecipientsExport($id_blast_chat),'Blast Chat Recipient - '.$blast_chat->blc_title.' - '.date('Y-m-d H-i-s').'.xlsx');
  }
  function recipient_import_excel(Request $request, $id_blast_chat)
  {
    $request->validate([
      'file_xls' => 'required'
    ]);
    $id_blast_chat = Main::decrypt($id_blast_chat);

    // saving file before loaded 
    $path = public_path().'/excel/';
    $request->file('file_xls')->move($path, 'imported_recipient.xlsx');
    $path = $path.'imported_recipient.xlsx';

    // loaded xlsx files  
    Excel::import(new RecipientsImport($id_blast_chat), $path);
    return 'import success';
  }

  function recipient_insert(Request $request, $id_blast_chat)
  {
    // validate 
    $request->validate([
      'nomor_whatsapp' => [ 'required',new PhoneNumber($request) ]
    ]);

    $id_blast_chat = Main::decrypt($id_blast_chat);  
    $blast_chat = mBlastChat::where('id_blast_chat', $id_blast_chat)->first();    
    $id_patient = $request->id_patient ? Main::decrypt($request->id_patient) : null;
    $id_country_call_code = $request->call_code ?  Main::decrypt($request->call_code) : ( $id_patient ? mPatient::where('id_patient', $id_patient)->first()->id_country_call_code : mCallCode::where('ccc_country_code','unknown')->first()->id_country_call_code);
    // validate 
    $request->validate([
      'nomor_whatsapp' => ['required', new PhoneNumber($request)]
    ]);
    $recipient = [];
    $call_code = 62;
    if($id_country_call_code) {
      $recipient['id_country_call_code'] = $id_country_call_code ?? mCallCode::where('ccc_call_code', $call_code)->first()->id_country_call_code;
      $country_call_code = mCallCode::where('id_country_call_code', $id_country_call_code)->first();
      $call_code = $country_call_code->ccc_call_code;
    }
    
    $recipient = array_merge($recipient,[  
      'bcr_whatsapp_number' => $id_patient ? $request->nomor_whatsapp : $call_code. $request->nomor_whatsapp ,
      'id_patient' => $id_patient,
      'id_country_call_code' => $id_country_call_code,
      'id_blast_chat' => $id_blast_chat,
      'bcr_variable_value' => json_encode($request->variable_content),
      'bcr_send_time' => $blast_chat->blc_send_time,
      'bcr_send_status' => 'not_yet',
      'bcr_message' => $request->hasil_chat,
      'bcr_recipient_type' => $request->id_patient ? 'patient_exist' : 'new'
    ]);
    mBlastChatRecipient::create($recipient);
  }

  function recipient_delete($id_recipient)
  {
    $id_recipient = Main::decrypt($id_recipient);
    mBlastChatRecipient::where('id_blast_chat_recipient', $id_recipient)->delete();
  }

  function recipient($id_blast_chat_raw)
  {
    $id_blast_chat = Main::decrypt($id_blast_chat_raw);
    $blast_chat = mBlastChat::where('id_blast_chat', $id_blast_chat)->first();
    $call_codes = mCallCode::get()->map(function($item){
      $item->id_encrypt = Main::encrypt($item->id_country_call_code);
      return $item; 
    });

    $data = Main::data($this->breadcrumb);
    $data['call_codes'] = $call_codes;
    $data['blast_chat'] = $blast_chat;
    $data['variables'] = json_decode($blast_chat->blc_variable_key);
    $data['send_time'] = Main::format_datetime($blast_chat->blc_send_time);
    $data['datatable_column'] = [
      ['data' => 'no'],
      ['data' => 'nomor_whatsapp'],
      ['data' => 'variable_content'],
      ['data' => 'message'],
      ['data' => 'jenis_penerima'],
      ['data' => 'waktu_kirim'],
      ['data' => 'status_kirim'],
      ['data' => 'menu']
    ];
    $data['id_blast_chat'] = $id_blast_chat_raw;


    return view('blastChat.recipient.recipientList', $data);
  }
}
