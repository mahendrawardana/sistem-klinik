<?php

namespace app\Http\Controllers\Control;

use app\Models\mAction;
use app\Models\mAppointment;
use app\Models\mControl;
use app\Models\mDone;
use app\Models\mPatient;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;
use Illuminate\Support\Facades\Session;

class Control extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['control'],
                'route' => ''
            ],
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $name = $request->name;
        $date_from = $request->date_from ? $request->date_from : date('01-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("t-m-Y", strtotime($date_from));
        $datatable_column = [
            ["data" => "no"],
            ["data" => "control_number"],
            ["data" => "name"],
            ["data" => "weight"],
            ["data" => "age"],
            ["data" => "control_location"],
            ["data" => "control_time"],
            ["data" => "options"],
        ];


        $data = array_merge($data, array(
            'datatable_column' => $datatable_column,
            'name' => $name,
            'date_from' => $date_from,
            'date_to' => $date_to,
        ));

        return view('control/control/controlList', $data);
    }

    function data_table(Request $request)
    {

        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $name = $request->name;

        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $total_data = mControl
            ::leftJoin('patient', 'patient.id_patient', '=', 'control.id_patient')
            ->whereIn('control.status', ['process'])
            ->whereBetween('control.control_time', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->where('patient.name', 'LIKE', '%' . $name . '%')
            ->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_control'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mControl
            ::leftJoin('patient', 'patient.id_patient', '=', 'control.id_patient')
            ->whereIn('control.status', ['process'])
            ->whereBetween('control.control_time', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->where('patient.name', 'LIKE', '%' . $name . '%')
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_control = Main::encrypt($row->id_control);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['control_number'] = $row->control_number;
            $nestedData['name'] = $row->name;
            $nestedData['weight'] = $row->weight . ' Kg';
            $nestedData['age'] = Main::format_age($row->birthday);
            $nestedData['control_location'] = Main::locationLabelView($row->control_location);
            $nestedData['control_time'] = Main::format_datetime($row->control_time);
            $nestedData['options'] = '
                
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-control_done dropdown-item btn-modal-general"
                           href="#"
                           data-route="' . route('controlDoneModal', ['id_control' => $id_control]) . '">
                            <i class="la la-check"></i>
                            Kontrol Selesai
                        </a>';

            if ($row->control_number == 1) {
                $nestedData['options'] .= '<a class="akses-control_back dropdown-item" href="' . route('controlCancelProcess', ['id_control' => $id_control, 'date_from' => $date_from, 'date_to' => $date_to, 'name' => $name]) . '">
                            <i class="la la-backward"></i>
                            Kembali Tindakan
                        </a>';
            }

            $nestedData['options'] .= '<div class="dropdown-divider"></div>
                        <a class="akses-control_edit dropdown-item btn-modal-general"
                           data-route="' . route('controlEditModal', ['id_control' => $id_control]) . '"
                           href="#">
                            <i class="la la-pencil"></i>
                            Edit
                        </a>
                        <a class="akses_control_detail dropdown-item btn-modal-general"
                           href="#"
                           data-route="' . route('controlDetail', ['id_control' => $id_control]) . '">
                            <i class="la la-info"></i>
                            Detail
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function done_modal($id_control)
    {
        $id_control = Main::decrypt($id_control);
        $control = mControl::where('id_control', $id_control)->first();
        $control_date_next = date('Y-m-d', strtotime($control->control_time . ' + 4 days'));
        $data = [
            'control' => $control,
            'control_date_next' => $control_date_next
        ];

        return view('control/control/controlDoneModal', $data);
    }

    function done_process(Request $request, $id_control)
    {
        $request->validate([
            'control_date' => 'required',
            'control_hours' => 'required',
        ]);

        $id_control = Main::decrypt($id_control);
        $control = mControl::where('id_control', $id_control)->first();

        $control_date = $request->control_date;
        $control_hours = $request->control_hours;
        $control_time = Main::format_date_db($control_date) . ' ' . Main::format_time_db($control_hours);

        $control_done_notes = $request->control_done_notes;
        $need_control = $request->need_control;
        $id_user = Session::get('user')['id'];
        $control_step = $control->control_number + 1;


        DB::beginTransaction();
        try {

            if ($need_control == 'yes') {

                /**
                 * Update data before
                 */
                mControl
                    ::where('id_control', $id_control)
                    ->update([
                        'control_done_time' => date('Y-m-d H:i:s'),
                        'status' => 'done'
                    ]);
                mPatient::where('id_patient', $control->id_patient)->update(['status' => 'control']);
                mAppointment::where('id_appointment', $control->id_appointment)->update(['status' => 'done']);

                /**
                 * Create Data Next
                 */
                $data_appointment = [
                    'id_patient' => $control->id_patient,
                    'id_user' => $id_user,
                    'need_type' => 'control',
                    'control_step' => $control_step,
                    'appointment_time' => $control_time,
                    'via' => 'system',
                    'status' => 'prepare'
                ];
                $id_appointment = mAppointment::create($data_appointment)->id_appointment;

                $data_control = [
                    'id_appointment' => $id_appointment,
                    'id_patient' => $control->id_patient,
                    'id_action' => $control->id_action,
                    'id_user' => $id_user,
                    'control_location' => $control->control_location,
                    'control_time' => $control_time,
                    'control_number' => $control_step,
                    'status' => 'process'
                ];

                $id_control_next = mControl::create($data_control)->id_control;
                mAppointment::where('id_appointment', $id_appointment)->update(['id_control' => $id_control_next]);


            } else {
                /**
                 * Update Data Before
                 */
                mControl
                    ::where('id_control', $id_control)
                    ->update([
                        'control_done_time' => date('Y-m-d H:i:s'),
                        'status' => 'done',
                        'control_done_notes' => $control_done_notes
                    ]);
                mAppointment::where('id_appointment', $control->id_appointment)->update(['status' => 'done']);

                /**
                 * Create Data Next
                 */
                $data_done = [
                    'id_control' => $id_control,
                    'id_patient' => $control->id_patient,
                    'id_user' => $id_user,
                    'done_time' => date('Y-m-d H:i:s'),
                    'status' => 'done'
                ];

                mDone::create($data_done);

                mPatient::where('id_patient', $control->id_patient)->update(['status' => 'done']);
            }

            DB::commit();

        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function cancel_process(Request $request, $id_control)
    {
        $id_control = Main::decrypt($id_control);
        $control = mControl::where('id_control', $id_control)->first();
        $id_appointment_action = mAction::where('id_action', $control->id_action)->value('id_appointment');
        $name = $request->name;
        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date('d-m-Y');

        DB::beginTransaction();
        try {
            mControl
                ::where('id_control', $id_control)
                ->update([
                    'status' => 'back',
                    'control_back_time' => date('Y-m-d H:i:s')
                ]);
            mAction
                ::where('id_action', $control->id_action)
                ->update([
                    'status' => 'process'
                ]);

            mPatient::where('id_patient', $control->id_patient)->update(['status' => 'action']);
            mAppointment::where('id_appointment', $control->id_appointment)->update(['status' => 'cancel']);
            mAppointment::where('id_appointment', $id_appointment_action)->update(['status' => 'prepare']);

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

        return redirect()->route('controlList', ['date_from' => $date_from, 'date_to' => $date_to, 'name' => $name]);
    }

    function edit_modal($id_control)
    {
        $id_control = Main::decrypt($id_control);
        $control = mControl::where('id_control', $id_control)->first();

        $data = [
            'control' => $control
        ];

        return view('control/control/controlEditModal', $data);
    }

    function update(Request $request, $id_control)
    {
        $request->validate([
            'control_location' => 'required',
            'control_number' => 'required',
            'control_date' => 'required',
            'control_hours' => 'required'
        ]);

        $id_control = Main::decrypt($id_control);
        $control_location = $request->control_location;
        $control_number = $request->control_number;
        $control_date = $request->control_date;
        $control_hours = $request->control_hours;
        $control_time = Main::format_date_db($control_date) . ' ' . Main::format_time_db($control_hours);
        $control = mControl::where('id_control', $id_control)->first();

        $data_update = [
            'control_location' => $control_location,
            'control_number' => $control_number,
            'control_time' => $control_time
        ];

        DB::beginTransaction();
        try {
            mControl
                ::where([
                    'id_control' => $id_control
                ])
                ->update($data_update);

            mAppointment
                ::where([
                    'id_appointment' => $control->id_appointment
                ])
                ->update([
                    'appointment_time' => $control_time
                ]);

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function detail($id_control)
    {
        $id_control = Main::decrypt($id_control);
        $row = mControl
            ::with([
                'patient',
                'patient.province',
                'patient.city',
                'patient.subdistrict',
                'patient.pekerjaan',
                'action',
                'action.consult',
                'action.consult.appointment',
                'action.appointment',
                'action.appointment',
                'control_list' => function ($query) {
                    $query->orderBy('control_number', 'ASC');
                }
            ])
            ->where('id_control', $id_control)
            ->first();

        $data = [
            'row' => $row
        ];

        return view('control/control/controlDetail', $data);
    }

}
