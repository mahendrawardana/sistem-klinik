<?php

namespace app\Http\Controllers\General;

use app\Models\mAction;
use app\Models\mAppointment;
use app\Models\mConsult;
use app\Models\mControl;
use app\Models\mPatient;
use app\Models\mPayment;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class Dashboard extends Controller
{
    private $breadcrumb = [
        [
            'label' => 'Dashboard',
            'route' => ''
        ]
    ];

    private $bulan = [
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'Nopember',
        '12' => 'Desember',
    ];

    function index(Request $request)
    {
//        return Session::all();
        $data = $this->data_dashboard_admin($request);

//        return $data['cart_patient'];

        return view('dashboard/dashboard_admin', $data);

    }

    function data_dashboard_admin($request)
    {
        $date_range_get = $request->input('date_range');
        $date_range = explode(' - ', $date_range_get);

        $date_from = $request->date_from ? $request->date_from : date('01-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("t-m-Y", strtotime($date_from));
        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $data = Main::data($this->breadcrumb);

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_appointment = mAppointment
            ::whereBetween('appointment_time', $where_date)
            ->whereIn('status', ['prepare', 'send'])
            ->count();
        $total_consult = mConsult
            ::whereBetween('consult_time', $where_date)
            ->whereIn('status', ['process'])
            ->count();
        $total_action = mAction
            ::whereBetween('action_time', $where_date)
            ->whereIn('status', ['process'])
            ->count();
        $total_control = mControl
            ::whereBetween('control_time', $where_date)
            ->whereIn('status', ['process'])
            ->count();
        $total_patient_all = mPatient
            ::count();
        $total_patient_done = mPatient
            ::whereBetween('created_at', $where_date)
            ->whereIn('status', ['done'])
            ->count();
        $total_patient_cancel = mPatient
            ::whereBetween('created_at', $where_date)
            ->where('status', ['cancel'])
            ->count();
        $total_payment = mPayment
            ::whereBetween('invoice_date', $where_date)
            ->sum('grand_total');
//        $total_payment = Main::format_money($total_payment);

        $start = new \DateTime($date_from_db);
        $end = new \DateTime($date_to_db);
        $end = $end->modify('+1 day');
        $interval = new \DateInterval('P1D');

        $label = [];
        $period = new \DatePeriod($start, $interval, $end);

        foreach ($period as $key => $value) {
            $label[] = $value->format('Y-m-d');
        }


        $cart_patient = [
            'label' => $label
        ];

        foreach ($period as $key => $value) {
            $date = $value->format('Y-m-d');

            $cart_patient['data']['appointment'][$date] =
                mAppointment
                    ::whereDate(
                        'appointment_time', $date
                    )
//                    ->whereIn('status', ['prepare','send','done'])
                    ->count();

            $cart_patient['data']['consult'][$date] =
                mConsult
                    ::whereDate('consult_time', $date)
//                    ->whereIn('status', ['process','done'])
                    ->count();

            $cart_patient['data']['action'][$date] =
                mAction
                    ::whereDate('action_time', $date)
//                    ->where([
//                        'status' => 'process'
//                    ])
                    ->count();

            $cart_patient['data']['control'][$date] =
                mControl
                    ::whereDate('control_time', $date)
//                    ->where([
//                        'status' => 'process'
//                    ])
                    ->count();

            $cart_patient['data']['patient'][$date] =
                mPatient
                    ::whereDate('created_at', $date)
                    ->count();

            $cart_patient['data']['payment'][$date] =
                mPayment
                    ::whereDate('invoice_date', $date)
                    ->sum('grand_total');
        }


        $data = array_merge($data, array(
            'total_appointment' => $total_appointment,
            'total_consult' => $total_consult,
            'total_action' => $total_action,
            'total_control' => $total_control,
            'total_patient_all' => $total_patient_all,
            'total_patient_done' => $total_patient_done,
            'total_patient_cancel' => $total_patient_cancel,
            'total_payment' => Main::format_money($total_payment),
            'date_from' => $date_from,
            'date_to' => $date_to,
            'cart_patient' => $cart_patient
        ));
        return $data;
    }

    function whatsapp_test()
    {
        Main::whatsappSend('+6281934364063', 'HELLO,, ini adalah test message ' . date('d-m-Y H:i:s'));
    }


}
