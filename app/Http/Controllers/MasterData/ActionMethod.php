<?php

namespace app\Http\Controllers\MasterData;

use app\Models\mAction;
use app\Models\mActionMethod;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;

class ActionMethod extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_4'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mActionMethod
            ::orderBy('title', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('masterData/actionMethod/actionMethodList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required'
        ]);

        $data = $request->except('_token');
        mActionMethod::create($data);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mActionMethod::where('id_action_method', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('masterData/actionMethod/actionMethodEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mActionMethod::where('id_action_method', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'title' => 'required',
            'description' => 'required'
        ]);
        $data = $request->except("_token");
        mActionMethod::where(['id_action_method' => $id])->update($data);
    }
}
