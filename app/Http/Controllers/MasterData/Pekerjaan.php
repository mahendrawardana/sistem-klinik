<?php

namespace app\Http\Controllers\MasterData;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;
use app\Models\mPekerjaan;

class Pekerjaan extends Controller
{
    private $breadcrumb;
    private $datatable_column = [
        [ "data" => "no" ],
        [ "data" => "nama pekerjaan" ],
        [ "data" => "menu" ],
    ];
    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_5'],
                'route' => ''
            ]
        ];
    }

    function index(){
        $data = Main::data($this->breadcrumb);
        $data['datatable'] = Main::create_datatable_tag(
            $this->datatable_column, 
            'akses-list datatable-new table table-striped table-bordered table-hover', 
            [ 'url' => route('jobDataTable')] 
        );
        return view('masterData.pekerjaan.pekerjaanList', $data);
    }
    function data_table(Request $request){
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_pekerjaan'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');
        $total_data = mPekerjaan::count() + 1; 
        $data_list = mPekerjaan::offset($start)->limit($limit)->orderBy($order_column, $order_type)->get();
        $option = ['keywords' => ''];
        $keywords = $option['keywords'];
        $start = $request->input('start');
        $order_type = $request->input('order.0.dir');

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }
            $nestedData = [];
            $nestedData['no'] = $no; 
            $nestedData['nama pekerjaan'] = $row->pkj_nama;
            $nestedData['menu'] =  '
            <div class="dropdown">
                <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                        type="button"
                        id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    Menu
                </button>
                <div class="dropdown-menu dropdown-menu-right"
                    aria-labelledby="dropdownMenuButton">
                    <a class="akses-action_wait_done dropdown-item btn-modal-general text-dark"
                        data-route="' .route('pekerjaanEditModal', Main::encrypt($row->id_pekerjaan)) . '">
                        <i class="la la-list"></i>
                        Edit
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item btn-hapus akses-hapus btn-hapus m--font-danger" 
                        data-route="' . route('pekerjaanDelete', Main::encrypt($row->id_pekerjaan)) . '" 
                        href="#"                        
                    >
                        <i class="la la-remove m--font-danger"></i> Delete
                    </a>
                </div>
            </div>
            ';           
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
            'keywords' => $keywords
        );

        return $json_data;        
    }

    function insert(Request $request){
        $request->validate(['pkj_nama' => 'required']);
        mPekerjaan::create(['pkj_nama' => $request->pkj_nama]);
    }

    function edit_modal($id_pekerjaan)
    {
        $id_pekerjaan_raw = $id_pekerjaan;
        $id_pekerjaan = Main::decrypt($id_pekerjaan);
        $pekerjaan = mPekerjaan::where('id_pekerjaan', $id_pekerjaan)->first();
        $pekerjaan->id_perkerjaan_en = $id_pekerjaan_raw;
        $data =[];
        $data['pekerjaan'] = $pekerjaan;
        return view('masterData.pekerjaan.pekerjaanEditModal', $data);
    }

    function update(Request $request,$id_pekerjaan)
    {
        $id_pekerjaan = Main::decrypt($id_pekerjaan);
        $request->validate(['pkj_nama' => 'required']);
        mPekerjaan::where('id_pekerjaan', $id_pekerjaan)->update($request->except('_token'));
    }

    function delete($id_pekerjaan){
        $id_pekerjaan = Main::decrypt($id_pekerjaan);
        return mPekerjaan::where('id_pekerjaan', $id_pekerjaan)->delete();
    }
}