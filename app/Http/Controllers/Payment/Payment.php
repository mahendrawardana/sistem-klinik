<?php

namespace app\Http\Controllers\Payment;

use app\Models\mAction;
use app\Models\mControl;
use app\Models\mDone;
use app\Models\mPayment;
use app\Models\mPaymentDetail;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;
use Illuminate\Support\Facades\Session;
use phpDocumentor\Reflection\DocBlock\Tags\See;

class Payment extends Controller
{
    private $breadcrumb;
    private $menu_active;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menu_active = $cons['payment'];
        $this->breadcrumb = [
            [
                'label' => $cons['payment'],
                'route' => route('paymentList')
            ]
        ];
    }

    function index(Request $request)
    {

        $data = Main::data($this->breadcrumb);
        $name = $request->name;
        $date_from = $request->date_from ? $request->date_from : date('01-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("t-m-Y", strtotime($date_from));
        $datatable_column = [
            ["data" => "no"],
            ["data" => "invoice_label"],
            ["data" => "invoice_date"],
            ["data" => "name"],
            ["data" => "age"],
//            ["data" => "address"],
            ["data" => "grand_total"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'datatable_column' => $datatable_column,
            'name' => $name,
            'date_from' => $date_from,
            'date_to' => $date_to
        ));

        return view('payment/payment/paymentList', $data);
    }

    function data_table(Request $request)
    {

        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $name = $request->name;

        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $total_data = mPayment
            ::leftJoin('patient', 'patient.id_patient', '=', 'payment.id_patient')
            ->with([
                'patient:id_patient,id_city,id_province,id_subdistrict',
                'patient.city',
                'patient.province',
                'patient.subdistrict',
            ])
            ->whereBetween('payment.invoice_date', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->where('patient.name', 'LIKE', '%' . $name . '%')
            ->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_payment'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mPayment
            ::leftJoin('patient', 'patient.id_patient', '=', 'payment.id_patient')
            ->whereBetween('payment.invoice_date', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->where('patient.name', 'LIKE', '%' . $name . '%')
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_payment = Main::encrypt($row->id_payment);
//            $address = $row->patient->province->province_name . ', ' . $row->patient->city->city_name . ', ' . $row->patient->subdistrict->subdistrict_name . ', ' . $row->address;

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['invoice_label'] = $row->invoice_label;
            $nestedData['invoice_date'] = Main::format_datetime($row->invoice_date);
            $nestedData['name'] = $row->name;
            $nestedData['age'] = Main::format_age($row->birthday);
//            $nestedData['address'] = $address;
            $nestedData['grand_total'] = Main::format_money($row->grand_total);
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill" type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-print dropdown-item" href="'.route('paymentPrint', ['id_payment' => $id_payment]).'" target="_blank">
                            <i class="la la-print"></i>
                            Cetak
                        </a>
                        <a class="akses-edit dropdown-item" href="'.route('paymentEdit', ['id_payment' => $id_payment]).'">
                            <i class="la la-pencil"></i>
                            Edit
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="akses-delete dropdown-item hidden" href="#" data-toggle="modal"
                           data-target="#modal-appointment-edit">
                            <i class="la la-remove"></i>
                            Hapus
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function edit($id_payment)
    {
        $breadcrumb = array_merge($this->breadcrumb, array(array(
            'label' => 'Edit',
            'route' => ''
        )));
        $data = Main::data($breadcrumb, $this->menu_active);


        $id_payment = Main::decrypt($id_payment);
        $payment = mPayment
            ::with('payment_detail')
            ->where('id_payment', $id_payment)
            ->first();

        $data = array_merge($data, array(
            'payment' => $payment
        ));

        return view('payment/payment/paymentEdit', $data);
    }

    function update(Request $request, $id_payment)
    {
        $request->validate([
            'description.*' => 'required',
        ]);

        $id_payment = Main::decrypt($id_payment);
        $id_user = Session::get('user')['id'];
        $description_arr = $request->description;
        $qty_arr = $request->qty;
        $price_arr = $request->price;
        $total = $request->total;
        $additional_cost = Main::format_number_db($request->payment_plus);
        $discount = Main::format_number_db($request->payment_cut);
        $grand_total = $request->grand_total;
        $id_action = $request->id_action;
        $id_patient = $request->id_patient;

        DB::beginTransaction();
        try {
            mPaymentDetail::where('id_payment', $id_payment)->delete();

            $data_payment = [
                'total' => $total,
                'additional_cost' => $additional_cost,
                'discount' => $discount,
                'grand_total' => $grand_total
            ];

            mPayment::where('id_payment', $id_payment)->update($data_payment);

            foreach ($description_arr as $key => $description) {
                $qty = Main::format_number_db($qty_arr[$key]);
                $price = Main::format_number_db($price_arr[$key]);

                $data_payment_detail = [
                    'id_payment' => $id_payment,
                    'id_action' => $id_action,
                    'id_patient' => $id_patient,
                    'id_user' => $id_user,
                    'description' => $description,
                    'qty' => $qty,
                    'price' => $price,
                    'total' => $qty * $price,
                ];

                mPaymentDetail::create($data_payment_detail);
            }

            DB::commit();

        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

    }

    function print($id_payment, $type = 'payment')
    {
        $id_payment = Main::decrypt($id_payment);
        if ($type == 'payment') {

        } elseif ($type == 'control') {
            $id_control = $id_payment;
            $id_action = mControl::where('id_control', $id_control)->value('id_action');
            $id_payment = mPayment::where('id_action', $id_action)->value('id_payment');
        }

        Session::put([
            'download_pdf' => 'no'
        ]);

        $payment = mPayment
            ::with([
                'payment_detail',
                'patient',
                'patient.province',
                'patient.city',
                'patient.subdistrict'
            ])
            ->where('id_payment', $id_payment)
            ->first();
        $karyawan = Session::get('user')['karyawan'];

        $data = [
            'payment' => $payment,
            'karyawan' => $karyawan
        ];


//        return view('payment/payment/paymentPrint');
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('payment/payment/paymentPrint', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Payment Print');
    }

    function export_excel(Request $request)
    {

        $date_from = $request->date_from;
        $date_to = $request->date_to;

        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $data_list = mPayment
            ::with([
                'patient',
                'patient.city',
                'patient.province',
                'patient.subdistrict',
            ])
            ->leftJoin('patient', 'patient.id_patient', '=', 'payment.id_patient')
            ->whereBetween('invoice_date', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->orderBy('name', 'ASC')
            ->get();

        $data = [
            'data_list' => $data_list,
            'date_from' => $date_from,
            'date_to' => $date_to
        ];

        return view('payment/payment/paymentExcel', $data);
    }

}
