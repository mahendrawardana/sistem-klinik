<?php

namespace app\Http\Controllers\ReminderSetting;

use app\Models\mReminderSetting;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;
use Nexmo\Response;

class ReminderSetting extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['reminder_setting'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $reminder_setting = mReminderSetting::orderBy('id_reminder_setting', 'ASC')->get();

        $data = array_merge($data, array(
            'reminder_setting' => $reminder_setting
        ));


        return view('reminderSetting/reminderSetting/reminderSettingList', $data);
    }

    function update(Request $request)
    {
        $status_arr = $request->status;
        $target_day_arr = $request->target_day;
        $target_hour_arr = $request->target_hour;

        foreach($target_day_arr as $id_reminder_setting => $target_day) {
            $data_update = [
                'status' => isset($status_arr[$id_reminder_setting]) ? 'active' : 'not_active',
                'target_day' => $target_day,
                'target_hour' => Main::format_time_db($target_hour_arr[$id_reminder_setting])
            ];

            mReminderSetting::where('id_reminder_setting', $id_reminder_setting)->update($data_update);

        }

//        return   [
//                $status_arr,
//                $target_day_arr,
//                $target_hour_arr
//            ];


    }

}
