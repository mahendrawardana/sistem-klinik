<?php

namespace app\Http\Controllers\ShceduleCalendar;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use app\Models\mAction;
use Illuminate\Support\Facades\Route;
use app\Models\mAppointment;
use app\Models\mConsult;
use app\Models\mControl;
use app\Models\mKaryawan;

class ShceduleCalendar extends Controller
{
    private $breadcrumb;
    private $view = [
        'nama_karyawan' => ["search_field" => "tb_karyawan.nama_karyawan"],
        'posisi_karyawan' => ["search_field" => "tb_karyawan.posisi_karyawan"],
        'telp_karyawan' => ["search_field" => "tb_karyawan.telp_karyawan"],
        'alamat_karyawan' => ["search_field" => "tb_karyawan.alamat_karyawan"],
        'email_karyawan' => ["search_field" => "tb_karyawan.email_karyawan"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['schedule_calendar'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $schedule_calendar_modal = Main::scheduleCalendarModal();

        $data = array_merge($data, [
            'schedule_calendar_modal' => $schedule_calendar_modal
        ]);
        

        return view('scheduleCalendar/scheduleCalendar/scheduleCalendarList', $data);
    }

    /**
     *
     * Supply Data for schedule calendar 
     *
     */
    function supply_data(Request $request) 
    {
        $start_date = $request->start;
        $end_date = $request->end;

        $start_datetime_db = Main::format_datetime_db($start_date); 
        $end_datetime_db = Main::format_datetime_db($end_date); 
        $appointments = collect(mAppointment
            ::leftJoin('patient', 'patient.id_patient', '=', 'appointment.id_patient')
                ->whereBetween('appointment_time',[$start_datetime_db, $end_datetime_db])
                ->get())
                ->map(function($appointment){
            $location = '';
            if($appointment->need_type == 'consult') {
                $location = mConsult
                    ::select('consult_location')
                        ->where('id_consult', $appointment->id_consult)
                        ->value('consult_location');
            } else if($appointment->need_type == 'action') {
                $location = mAction
                    ::select('action_location')
                        ->where('id_action', $appointment->id_action)
                        ->value('action_location');
            } else if($appointment->need_type == 'control') {
                $location = mControl
                    ::select('control_location')
                        ->where('id_control', $appointment->id_control)
                        ->value('control_location');
            }

            $location = Main::locationLabelView($location);


            return [
                'title' => Main::need_type_label($appointment->need_type, $appointment->control_step).' - '. $appointment->name .' - '. $location,
                'description' => Main::need_type_label($appointment->need_type, $appointment->control_step).' - '. $appointment->name.' - '. $location.' - '.$appointment->weight.' Kg - '.Main::format_age($appointment->birthday).' - '.Main::format_time_label($appointment->appointment_time),
                'className' => Main::need_type_class($appointment->need_type),
                'start' => $appointment->appointment_time, 
            ];
        });

        return json_decode($appointments); 
    }
}
