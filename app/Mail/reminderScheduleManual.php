<?php

namespace app\Mail;

use app\Helpers\Main;
use app\Models\mAppointment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class reminderScheduleManual extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $need_type = "";
    public $from_date = "";
    public $to_date = "";

    public function __construct($type, $from = "", $to = "")
    {
        //        
        $this->need_type = $type;
        $this->from_date = $from;
        $this->to_date = $to;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $date_filter_title = Main::format_date_label($this->from_date) . ' - ' . Main::format_date_label($this->to_date);
        $appointment = mAppointment
            ::select([
                'appointment.*',
                'patient.*',
                'appointment.status AS appointment_status',
                'action_method.title AS action_method_title'
            ])
            ->leftJoin('patient', 'patient.id_patient', '=', 'appointment.id_patient')
            ->leftJoin('control', 'control.id_control', '=', 'appointment.id_control')
            ->leftJoin('action', 'action.id_action', '=', 'control.id_action')
            ->leftJoin('action_method', 'action_method.id_action_method', '=', 'action.id_action_method')
            ->whereBetween('appointment_time', [$this->from_date . ' 00:00:00', $this->to_date . ' 23:59:00'])
            ->whereIn('appointment.status', ['prepare', 'send'])
            ->where('appointment.need_type', $this->need_type)
            ->orderBy('appointment_time', 'ASC')
            ->get();
        $data = [
            'appointment' => $appointment,
            'date_filter' => $date_filter_title,
            'need_type' => $this->need_type
        ];
        return $this
            ->subject('Jadwal ' . ucwords($this->need_type) .' - '. $date_filter_title . ' - Rumah Sunat Bali')
            ->from('no-reply@hubpasien.com', 'Hub Pasien')
            ->view('reminderSchedule.reminderSchedule')
            ->with($data);
    }
}
