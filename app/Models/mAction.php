<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mAction extends Model
{
    use SoftDeletes;

    protected $table = 'action';
    protected $primaryKey = 'id_action';
    protected $fillable = [
        'id_action',
        'id_patient',
        'id_consult',
        'id_appointment',
        'id_user',
        'id_action_method',
        'action_location',
        'action_time',
        'back_time',
        'done_time',
        'cancel_time',
        'action_cancel_notes',
        'action_done_notes',
        'status',
        'whatsapp_status_short',
        'whatsapp_status_long',
    ];

    function patient() {
        return $this->belongsTo(mPatient::class, 'id_patient', 'id_patient');
    }

    function appointment() {
        return $this->belongsTo(mAppointment::class, 'id_appointment', 'id_appointment');
    }

    function consult() {
        return $this->belongsTo(mConsult::class, 'id_consult', 'id_consult');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
