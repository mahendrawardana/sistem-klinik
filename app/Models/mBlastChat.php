<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mBlastChat extends Model
{
    use SoftDeletes;

    protected $table = 'blast_chat';
    protected $primaryKey = 'id_blast_chat';
    protected $fillable = [
        'blc_title', 
        'blc_notes',   
        'blc_variable_key',   
        'blc_chat_template',   
        'blc_send_time',
        'blc_run_status',
    ];

    function blast_chat_recipient(){
      return $this->hasMany(mBlastChatRecipient::class,'id_blast_chat');
    }
}
