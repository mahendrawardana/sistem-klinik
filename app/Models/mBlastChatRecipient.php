<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use app\Models\mPatient;

class mBlastChatRecipient extends Model
{
  use SoftDeletes;

  protected $table = 'blast_chat_recipient';
  protected $primaryKey = 'id_blast_chat_recipient';
  protected $fillable = [
    'id_blast_chat',
    'id_patient',
    'id_country_call_code',
    'bcr_whatsapp_number',
    'bcr_variable_value',
    'bcr_message',
    'bcr_recipient_type',
    'bcr_send_status',
    'bcr_send_time'
  ];

  function patient()
  {
    return $this->hasOne(mPatient::class, 'id_patient', 'id_patient');
  }

  function blast_chat()
  {
    return $this->belongsTo(mBlastChat::class, 'id_blast_chat');
  }
}
