<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mCallCode extends Model
{
    protected $table = 'country_call_code';
    protected $primaryKey = 'id_country_call_code';
    protected $fillable = [];
}