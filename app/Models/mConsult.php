<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mConsult extends Model
{
    use SoftDeletes;

    protected $table = 'consult';
    protected $primaryKey = 'id_consult';
    protected $fillable = [
        'id_patient',
        'id_appointment',
        'id_user',
        'consult_location',
        'consult_time',
        'cancel_time',
        'done_time',
        'status',
        'consult_done_notes',
        'consult_cancel_notes',
    ];

    function patient() {
        return $this->belongsTo(mPatient::class, 'id_patient', 'id_patient');
    }

    function appointment() {
        return $this->belongsTo(mAppointment::class, 'id_appointment', 'id_appointment');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
