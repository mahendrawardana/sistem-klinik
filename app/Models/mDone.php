<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mDone extends Model
{
    use SoftDeletes;

    protected $table = 'done';
    protected $primaryKey = 'id_done';
    protected $fillable = [
        'id_control',
        'id_patient',
        'id_action',
        'id_user',
        'done_time',
        'status',
        'back_time'
    ];

    function patient() {
        return $this->belongsTo(mPatient::class, 'id_patient');
    }

    function control() {
        return $this->belongsTo(mControl::class, 'id_control');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
