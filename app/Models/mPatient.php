<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPatient extends Model
{
    use SoftDeletes;

    protected $table = 'patient';
    protected $primaryKey = 'id_patient';
    protected $fillable = [
        'id_province',
        'id_country_call_code',
        'id_pekerjaan',
        'id_city',
        'id_subdistrict',
        'id_user',
        'name',
        'religion',
        'birthday',
        'weight',
        'address',
        'phone_1',
        'phone_2',
        'study',
        'place_to_study',
        'status',
    ];

    function city() {
        return $this->belongsTo(mCity::class, 'id_city', 'id_city');
    }

    function subdistrict() {
        return $this->belongsTo(mSubdistrict::class, 'id_subdistrict', 'id_subdistrict');
    }

    function province() {
        return $this->belongsTo(mProvince::class, 'id_province', 'id_province');
    }

    function medic_record() {
        return $this->belongsTo(mMedicRecord::class, 'id_patient', 'id_patient');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function pekerjaan(){
        return $this->belongsTo(mPekerjaan::class,'id_pekerjaan', 'id_pekerjaan');
    }
}
