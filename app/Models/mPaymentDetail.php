<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPaymentDetail extends Model
{
    use SoftDeletes;

    protected $table = 'payment_detail';
    protected $primaryKey = 'id_payment_detail';
    protected $fillable = [
        'id_payment',
        'id_action',
        'id_patient',
        'id_user',
        'description',
        'qty',
        'price',
        'total'
    ];

    function barang() {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
