<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPekerjaan extends Model
{
    protected $table = 'pekerjaan';
    protected $primaryKey = 'id_pekerjaan';
    protected $fillable = [
        'pkj_nama',
    ];

    function patient(){
        $this->hasMany(mPatient::class, 'id_patient','id_patient');
    }
}