<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mWhatsApp extends Model
{
    use SoftDeletes;

    protected $table = 'whatsapp';
    protected $primaryKey = 'id_whatsapp';
    protected $fillable = [
        'id_whatsapp',
        'payment',
        'total_chat_quote',
        'total_chat_send',
        'success',
        'description',
        'result_code'
    ];

}
