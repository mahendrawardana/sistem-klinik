<?php

namespace app\Rules;

use Illuminate\Contracts\Validation\Rule;

class validateVariableName implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
      // check key name has ' ` or " 
      $regex = "/['\"`]/";
      preg_match_all($regex, implode("",$value), $matches);
      return count($matches[0]) < 1;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
      return '<strong>nama variable</strong> tidak boleh mengandung <strong>\'</strong>,<strong>"</strong>, atau <strong>`</strong>';
    }
}
