<?php
/**
 * Created by PhpStorm.
 * User: mahendrawardana
 * Date: 07/02/19
 * Time: 13.36
 */

return [
    'acc_api_token' => 'c890cc0bd6562ed5b3a2a1105d6ddb9af9fdadab0c55216255bb0465d23c604aab845955b7b43787392c108f5a8cc82292435c99e7e93720145d89193cac06ba',
    'prefixProduksi' => 'MKK-',
    'ppnPersen' => 0.1,
    'decimalStep' => .01,
    'kodeHutangSupplier' => 'HS',
    'kodePreOrder' => 'PO',
    'kodeHutangLain' => 'HL',
    'kodePiutangPelanggan' => 'PP',
    'kodePiutangLain' => 'PL',
    'kodePenyesuaianStokBahan' => 'PSB',
    'kodePenyesuaianStokProduk' => 'PSP',
    'kodeJurnalUmum' => 'JU',
    'kodeAsset' => 'AT',
    'kodeKategoryAsset' => 'KAS',
    'bankType' => 'BCA',
    'bankRekening' => '4161877888',
    'bankAtasNama' => 'BAMBANG PRANOTO',
    'companyName' => 'Sistem Klinik',
    'companyAddress' => 'JL. Jendral A Yani, No. 04, Baler Bale Agung, Negara',
    'companyPhone' => '0818 0260 9624',
    'companyTelp' => '(0365) 41100',
    'companyEmail' => 'kutuskutusbali@gmail.com',
    'companyBendahara' => 'ARNIEL',
    'companyTuju' => 'Bapak Angga',
    'topMenu' => [
        'dashboard' => 'dashboard',

        'appointment' => 'appointment',
        'patient' => 'data_pasien',
        'consult' => 'konsultasi',
        'action' => 'tindakan',
        'control' => 'kontrol',
        'done' => 'selesai',
        'payment' => 'payment',
        'reminder' => 'reminder',
        'reminder_setting' => 'pengaturan_reminder',
        'reminder_history' => 'riwayat_reminder',
        'reminder_message' => 'pesan_reminder',
        'schedule_calendar' => 'kalender_jadwal',
        'whatsapp_link' => 'link_whatsapp',
        'blast_chat' => 'blast_chat',
        'masterData' => 'master_data',
        'master_1' => 'staff',
        'master_2' => 'role_user',
        'master_3' => 'user',
        'master_4' => 'metode_tindakan',
        'master_5' => 'pekerjaan',
    ]
];
