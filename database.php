<?php

//use Illuminate\Http\Request;
//error_reporting(-1);
//$headers = apache_request_headers();

//echo json_encode(request()->all());

//echo json_encode(request()->all());

//echo $_POST['username'];
//echo $_POST['password'];

//echo json_encode(\Illuminate\Support\Facades\Request::post('username'));
//echo \Illuminate\Http\Request::input('username');

//echo Request::header('Authorization');

//if (Request::header('Authorization')) {
//    echo '1';
//    echo Request::header('Authorization');
//} else {
//    $username = $_POST['username'];
//    $password = $_POST['password'];
//    $password_encrypt = \Illuminate\Support\Facades\Hash::make($password);
//
//
//
//}


$headers = apache_request_headers();
$token_separator = '**';
$response_auth = [
    'status' => 'auth_error',
    'message' => 'Sesi login/token sudah berubah',
    'data' => [],
    'errors' => []
];
$response_akun = [
    'status' => 'error',
    'message' => 'Isian field belum benar',
    'data' => null,
    'errors' => [
        'password' => [
            "Username atau Password salah di root"
        ]
    ]
];

$database_name = env('DB_DEMO_DATABASE');

/**
 * Untuk get database name, berdasarkan post login
 */


$username = '';
$password = '';
$status_get_database_name = FALSE;

/**
 * Check jika post username dan post password ada,
 * biasanya terjadi di mobile atau postman
 */
if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
} else {
    $data = json_decode(file_get_contents('php://input'), true);
//    print_r($data);

    if (isset($data['username']) && isset($data['password'])) {

        if ($data['username'] == '' && $data['password'] == '') {
            response_json([
                'status' => 'error',
                'message' => 'Isian field belum benar',
                'data' => null,
                'errors' => [
                    'username' => [
                        'Username harus diisi'
                    ],
                    'password' => [
                        'Password harus diisi'
                    ],
                ]
            ]);
        } elseif($data['username'] == '') {
            response_json([
                'status' => 'error',
                'message' => 'Isian field belum benar',
                'data' => null,
                'errors' => [
                    'username' => [
                        'Username harus diisi'
                    ],
                ]
            ]);
        } elseif($data['password'] == '') {
            response_json([
                'status' => 'error',
                'message' => 'Isian field belum benar',
                'data' => null,
                'errors' => [
                    'password' => [
                        'Password harus diisi'
                    ],
                ]
            ]);
        } else {
            $username = $data['username'];
            $password = $data['password'];
        }
    }
}

/**
 * jika username dan password kosong dari metode pos
 * maka di coba di dapatkan dari post data input,
 * yang biasanya terjadi di react js web
 *
 * alur:
 * 1. jika username dan password kosong
 * 2. mengecheck apakah ada token atau tidak (biasanya terjadi di react js web)
 * 3. jika null, maka get data post dari php input
 * 4. jika array key username dan password ada, maka coba get data username dan password
 * 5. jika array value username dan password tidak kosong, maka masukkan ke dalam variable
 * 6. jika kosong, maka check apakah punya username atau tidak
 * 7. response error tersebut
 */


//if ($username == '' && $password == '') {
////    echo '1';
//    $headers = apache_request_headers();
////    response_json($headers['Authorization']);
//    $authorization = $headers['Authorization'];
//////    echo '2';
//    $key = explode(' ', $authorization);
//
////    response_json($key);
////    echo '3';
//    $prefix = isset($key[0]) ? $key[0] : '';
//    $token = isset($key[1]) ? $key[1] : '';
////    echo '4';
////
//
//    if ($token == 'null') {
////        echo '5';
//        $data = json_decode(file_get_contents('php://input'), true);
////        print_r($data);
////        echo '6';
////        echo $data['username'].' - '.$data['password'];
//        if (isset($data['username']) && isset($data['password'])) {
//
//            if ($data['username'] != '' && $data['password'] != '') {
//                $username = $data['username'];
//                $password = $data['password'];
//            } else {
//                if ($data['username'] == '') {
//                    response_json([
//                        'status' => 'error',
//                        'message' => 'Isian field belum benar',
//                        'data' => null,
//                        'errors' => [
//                            'username' => [
//                                'Username harus diisi'
//                            ],
//                        ]
//                    ]);
//                } elseif ($data['password'] == '') {
//                    response_json([
//                        'status' => 'error',
//                        'message' => 'Isian field belum benar',
//                        'data' => null,
//                        'errors' => [
//                            'password' => [
//                                "Password harus diisi"
//                            ]
//                        ]
//                    ]);
//                }
//            }
//        }
//    }
//}
//echo '4';

//echo $username.' -- '.$password;

/**
 * Untuk get database name, berdasarkan username dan password yg baru login
 */
if ($username !== '' && $password !== '') {
    $DB_MASTER_HOST = env('DB_MASTER_HOST');
    $DB_MASTER_USERNAME = env('DB_MASTER_USERNAME');
    $DB_MASTER_PASSWORD = env('DB_MASTER_PASSWORD');
    $DB_MASTER_DATABASE = env('DB_MASTER_DATABASE');

    $conn_master = mysqli_connect($DB_MASTER_HOST, $DB_MASTER_USERNAME, $DB_MASTER_PASSWORD, $DB_MASTER_DATABASE);

    $member_config = mysqli_query($conn_master, "SELECT database_name FROM member_config WHERE status = 'active'");

    $status_login = FALSE;

    while ($row_member_config = mysqli_fetch_array($member_config)) {
        $database_name = $row_member_config['database_name'];
//        $db_token = $row_member_config['db_token'];

        $DB_HOST = env('DB_HOST');
        $DB_USERNAME = env('DB_USERNAME');
        $DB_PASSWORD = env('DB_PASSWORD');

        $conn_company = mysqli_connect($DB_HOST, $DB_USERNAME, $DB_PASSWORD, $database_name);
        $check_username = mysqli_num_rows(mysqli_query($conn_company, "SELECT staff_username FROM staff WHERE staff_username = '$username' AND deleted_at IS NULL"));

        if ($check_username == 1) {
            $status_login = TRUE;
            $status_get_database_name = TRUE;
            mysqli_close($conn_company);
            break;
        }

    }

    mysqli_close($conn_master);

    if (!$status_login) {
        response_json($response_akun);
    }

}

/**
 * untuk get database name, berdasarkan header Authorization
 */

if (isset($headers['authorization']) && !$status_get_database_name) {
    $authorization = $headers['Authorization'];
    $key = explode(' ', $authorization);

    $prefix = isset($key[0]) ? $key[0] : '';
    $token = isset($key[1]) ? $key[1] : '';

    $database_name = get_database_name_header($token_separator, $response_auth, $prefix, $token);
}

/**
 * Untuk get dadtabase name, berdasarkan URL yang berisi parameter GET dengan prefix dan token
 */
if (isset($_GET['prefix']) && isset($_GET['token']) && !$status_get_database_name) {

    $prefix = isset($_GET['prefix']) ? $_GET['prefix'] : '';
    $token = isset($_GET['token']) ? $_GET['token'] : '';

    $database_name = get_database_name_header($token_separator, $response_auth, $prefix, $token);

}

/**
 * Proses untuk mendapatkan database name, dari prefix dan token keseluruhan (gabungan antara token_staff & token_db dengan separator variable diatas)
 * Kondisi yang sedang berjalan
 *
 * 1. mengecheck apakah prefix sudah benar dan token ada atau belum
 * 2. jika ada, maka split token untuk mendapatkan token_staff & token_db
 * 3. jika sudah, maka dilakukan pengecheckan, apakah token_db ada atau tidak,
 * 4. jika ada, maka get_database_name berdasarkan token
 * 5. jika tidak ada, maka munculkan error auth
 * 6. selesai
 *
 * @param $token_separator
 * @param $response_auth
 * @param $prefix
 * @param $token
 * @return mixed
 */
function get_database_name_header($token_separator, $response_auth, $prefix, $token)
{
    if ($prefix == 'bearer' && $token) {
        $token_user = explode($token_separator, $token);
        $token_staff = $token_user[0];
        $token_db = $token_user[1];

        $DB_MASTER_HOST = env('DB_MASTER_HOST');
        $DB_MASTER_USERNAME = env('DB_MASTER_USERNAME');
        $DB_MASTER_PASSWORD = env('DB_MASTER_PASSWORD');
        $DB_MASTER_DATABASE = env('DB_MASTER_DATABASE');

        $conn_master = mysqli_connect($DB_MASTER_HOST, $DB_MASTER_USERNAME, $DB_MASTER_PASSWORD, $DB_MASTER_DATABASE);

        $check_db_exist = mysqli_num_rows(mysqli_query($conn_master, "SELECT member_config_id FROM member_config WHERE db_token = '$token_db'"));
        if ($check_db_exist == 1) {
            $member_config = mysqli_query($conn_master, "SELECT database_name FROM member_config WHERE db_token = '$token_db'");
            $data = mysqli_fetch_row($member_config);
            mysqli_close($conn_master);

//            echo json_encode($data[0]);

            $database_name = $data[0];

            return $database_name;
        } else {
            response_json($response_auth);
        }

    } else {
        response_json($response_auth);
    }
}

function response_json($data)
{
    header('Content-Type: application/json');
    echo json_encode($data);
    exit;
}

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => env('DB_PREFIX', ''),
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', 3306),
            'database' => $database_name,//env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => env('DB_CHARSET', 'utf8mb4'),
            'collation' => env('DB_COLLATION', 'utf8mb4_unicode_ci'),
            'prefix' => env('DB_PREFIX', ''),
            'strict' => env('DB_STRICT_MODE', true),
            'engine' => env('DB_ENGINE', null),
            'timezone' => env('DB_TIMEZONE', '+00:00'),
        ],

        'mysql_master' => [
            'driver' => 'mysql',
            'host' => env('DB_MASTER_HOST', '127.0.0.1'),
            'port' => env('DB_MASTER_PORT', 3306),
            'database' => env('DB_MASTER_DATABASE', 'forge'),
            'username' => env('DB_MASTER_USERNAME', 'forge'),
            'password' => env('DB_MASTER_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => env('DB_CHARSET', 'utf8mb4'),
            'collation' => env('DB_COLLATION', 'utf8mb4_unicode_ci'),
            'prefix' => env('DB_PREFIX', ''),
            'strict' => env('DB_STRICT_MODE', true),
            'engine' => env('DB_ENGINE', null),
            'timezone' => env('DB_TIMEZONE', '+00:00'),
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', 5432),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => env('DB_CHARSET', 'utf8'),
            'prefix' => env('DB_PREFIX', ''),
            'schema' => env('DB_SCHEMA', 'public'),
            'sslmode' => env('DB_SSL_MODE', 'prefer'),
        ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', 1433),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => env('DB_CHARSET', 'utf8'),
            'prefix' => env('DB_PREFIX', ''),
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'cluster' => env('REDIS_CLUSTER', false),

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_DB', 0),
        ],

        'cache' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_CACHE_DB', 1),
        ],

    ],

];

if(!function_exists('apache_request_headers')) {
    function apache_request_headers() {

        // Based on: http://www.iana.org/assignments/message-headers/message-headers.xml#perm-headers
        $arrCasedHeaders = array(
            // HTTP
            'Dasl'             => 'DASL',
            'Dav'              => 'DAV',
            'Etag'             => 'ETag',
            'Mime-Version'     => 'MIME-Version',
            'Slug'             => 'SLUG',
            'Te'               => 'TE',
            'Www-Authenticate' => 'WWW-Authenticate',
            // MIME
            'Content-Md5'      => 'Content-MD5',
            'Content-Id'       => 'Content-ID',
            'Content-Features' => 'Content-features',
        );
        $arrHttpHeaders = array();

        foreach($_SERVER as $strKey => $mixValue) {
            if('HTTP_' !== substr($strKey, 0, 5)) {
                continue;
            }

            $strHeaderKey = strtolower(substr($strKey, 5));

            if(0 < substr_count($strHeaderKey, '_')) {
                $arrHeaderKey = explode('_', $strHeaderKey);
                $arrHeaderKey = array_map('ucfirst', $arrHeaderKey);
                $strHeaderKey = implode('-', $arrHeaderKey);
            }
            else {
                $strHeaderKey = ucfirst($strHeaderKey);
            }

            if(array_key_exists($strHeaderKey, $arrCasedHeaders)) {
                $strHeaderKey = $arrCasedHeaders[$strHeaderKey];
            }

            $arrHttpHeaders[$strHeaderKey] = $mixValue;
        }

        return $arrHttpHeaders;

    }
}

