<form action="{{ route('actionCancel',$id_action) }}" method="post"
    class="form-send">

  {{ csrf_field() }}


  <div class="modal" id="modal-general" role="dialog"
       aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">
                      <i class="la la-pencil"></i> Cancel
                  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <div class="form-group m-form__group row">
                      <label class="col-lg-4 col-form-label">Alasan Cancel</label>
                      <div class="col-lg-8">
                          <input class="form-control m-input m-input--pill"
                                 name="alasan_cancel" autocomplete="off" type="text" value="">
                      </div>
                  </div>
              </div>
              @foreach ($data as $key => $item)
                <input type="hidden" name="{{$key}}" value="{{$item}}">
              @endforeach
              <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Lanjutkan</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
              </div>
          </div>
      </div>
  </div>
</form>