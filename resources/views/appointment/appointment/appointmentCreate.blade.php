<form action="{{ route('appointmentMemberNewInsert') }}" method="post" data-alert-show="true"
    data-alert-field-message="true" data-alert-show-success-status="false"
    data-alert-show-success-title="Appointment Terkirim"
    data-alert-show-success-message="Berhasil mengirim appointment untuk ditindak lanjuti"
    class="m-form m-form--fit m-form--label-align-right form-send">

    {{ csrf_field() }}
    <input type="hidden" name="via" value="admin">

    <div class="modal" id="modal-appointment-create" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        <i class="la la-plus"></i> Tambah Appointment
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Keperluan</label>
                        <div class="col-lg-8">
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="need_type" value="consult" checked> Konsultasi
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="need_type" value="action"> Tindakan
                                    <span></span>
                                </label>
                                {{--                                <label class="m-radio"> --}}
                                {{--                                    <input type="radio" name="need_type" --}}
                                {{--                                           value="control"> Kontrol --}}
                                {{--                                    <span></span> --}}
                                {{--                                </label> --}}
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="from" value="admin">
                    <div class="form-group m-form__group row wrapper-control-step hidden">
                        <label class="col-lg-4 col-form-label">Kontrol ke ?</label>
                        <div class="col-lg-2">
                            <input class="form-control m-input m-input--pill" name="control_step" id="m_touchspin_4"
                                value="" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Lokasi Klinik Tujuan</label>
                        <div class="col-lg-8">
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="location" value="denpasar" checked> Denpasar
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="location" value="nusa_dua"> Nusa Dua
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="location" value="gianyar"> Gianyar
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="location" value="home_service"> Home Service
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Nama</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill" name="name" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Agama</label>
                        <div class="col-lg-8">
                            <select class="form-control" name="religion">
                                <option value="islam">Islam</option>
                                <option value="protestan">Protestan</option>
                                <option value="katolik">Katolik</option>
                                <option value="hindu">Hindu</option>
                                <option value="buddha">Buddha</option>
                                <option value="khonghucu">Khonghucu</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Pekerjaan</label>
                        <div class="col-lg-8">
                            <select class="form-control" name="pekerjaan">
                                <option value="">Pilih Pekerjaan</option>
                                @foreach ($pekerjaan as $item)
                                    <option value="{{ $item->id_pekerjaan }}">{{ $item->pkj_nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Pendidikan</label>
                        <div class="col-lg-8">
                            <select class="form-control" name="study">
                                <option value="">Pilih Pendidikan Terhakhir</option>
                                <option value="sd">SD</option>
                                <option value="smp">SMP</option>
                                <option value="sma/smk">SMA/SMK</option>
                                <option value="diploma">Diploma</option>
                                <option value="sarjana">Sarjana</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Tempat Menempuh Pendidikan</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill" name="place_to_study" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Tanggal Lahir</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_datepicker_1_modal" name="birthday"
                                type="text" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Provinsi</label>
                        <div class="col-lg-8">
                            <select class="form-control m-input m-input--pill m-select2" name="id_province"
                                data-placeholder="Pilih Provinsi" style="width: 100%">
                                <option value="">Pilih Provinsi</option>
                                @foreach ($province as $row)
                                    <option value="{{ $row->id_province }}">{{ $row->province_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Kabupaten</label>
                        <div class="col-lg-8">
                            <select class="form-control m-input" name="id_city">
                                <option value="">Pilih Kabupaten</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Kecamatan</label>
                        <div class="col-lg-8">
                            <select class="form-control m-input" name="id_subdistrict">
                                <option value="">Pilih Kecamatan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Alamat Detail</label>
                        <div class="col-lg- 8">
                            <textarea class="form-control m-input m-input--pill" name="address" placeholder="Jln. Gatsu Barat, No 3 Desa Gatsu"></textarea>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Berat Badan</label>
                        <div class="col-lg-2">
                            <input class="form-control m-input m-input--pill" name="weight" id="m_touchspin_4"
                                value="55" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Jadwal Kalender</label>
                        <div class="col-lg-8">
                            <a href="{{ route('scheduleCalendarPage') }}" class="btn btn-primary m-btn--pill"
                                target="_blank">
                                <i class="la la-eye"></i> Lihat Jadwal
                            </a>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Tanggal Appointment</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_datepicker_1_modal"
                                name="appointment_date" autocomplete="off" type="text"
                                value="{{ date('d-m-Y') }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Jam Appointment</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_timepicker_1_modal"
                                name="appointment_hours" autocomplete="off" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">No HP/WhatsApp</label>
                        <div class="col-lg-8">
                            <div class="row w-100">
                                <select class="form-control col-5" name="call_code">
                                    <option value="62">{{ 'Indonesia (+62)' }}</option>
                                    <optgroup label="Other Countries">
                                        @foreach ($call_codes as $call_code)
                                            <option value="{{ $call_code->ccc_call_code }}">
                                                {{ $call_code->ccc_country_name }} (
                                                {{ "+$call_code->ccc_call_code" }} )</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                                <input class="form-control m-input m-input--pill col-7" name="phone_1"
                                    type="text">
                            </div>
                            <span class="m-form__help">
                                Contoh Pengisian Nomer Telepon: <strong>81934364063</strong>
                            </span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">No HP Lainnya</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill col-9" name="phone_2" type="text">
                            <span class="m-form__help">
                                Contoh Pengisian Nomer Telepon: <strong>628273826725</strong>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </div>
    </div>

</form>
