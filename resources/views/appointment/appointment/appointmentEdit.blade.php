<form action="{{ route('appointmentUpdate', ['id_appointment' => Main::encrypt($edit->id_appointment)]) }}" method="post"
    class="form-send" data-alert-show-success-status="false" data-alert-show-success-title="Appointment Terkirim"
    data-alert-show-success-message="Berhasil mengirim appointment untuk ditindak lanjuti">
    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">

        {{ csrf_field() }}

        <input type="hidden" name="id_patient" value="{{ $edit->id_patient }}">
        <input type="hidden" name="id_appointment" value="{{ $edit->id_appointment }}">

        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        <i class="la la-pencil"></i> Edit Appointment
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Keperluan</label>
                        <div class="col-lg-8">
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="need_type" value="consult"
                                        {{ $edit->need_type == 'control' ? 'disabled' : '' }}
                                        {{ $edit->need_type == 'consult' ? 'checked' : '' }}>
                                    Konsultasi
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="need_type" value="action"
                                        {{ $edit->need_type == 'control' ? 'disabled' : '' }}
                                        {{ $edit->need_type == 'action' ? 'checked' : '' }}>
                                    Tindakan
                                    <span></span>
                                </label>
                                <label class="m-radio {{ $edit->need_type != 'control' ? 'hidden' : '' }}">
                                    <input type="radio" name="need_type" value="control"
                                        {{ $edit->need_type == 'control' ? 'checked' : '' }}>
                                    Kontrol
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div
                        class="form-group m-form__group row wrapper-control-step {{ $edit->need_type == 'control' ? '' : 'hidden' }}">
                        <label class="col-lg-4 col-form-label">Kontrol ke ?</label>
                        <div class="col-lg-2">
                            <input class="form-control m-input m-input--pill m-touchspin" name="control_step"
                                value="{{ $edit->control_step ? $edit->control_step : 1 }}" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Lokasi Klinik Tujuan</label>
                        <div class="col-lg-8">
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="location" value="denpasar"
                                        {{ $location == 'denpasar' ? 'checked' : '' }}> Denpasar
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="location" value="nusa_dua"
                                        {{ $location == 'nusa_dua' ? 'checked' : '' }}> Nusa Dua
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="location" value="gianyar"
                                        {{ $location == 'gianyar' ? 'checked' : '' }}> Gianyar
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="location" value="home_service"
                                        {{ $location == 'home_service' ? 'checked' : '' }}> Home Service
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Nama</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill" name="name" type="text"
                                value="{{ $edit->patient->name }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Agama</label>
                        <div class="col-lg-8">
                            <select class="form-control" name="religion">
                                <option value="islam" {{ $edit->patient->religion == 'islam' ? 'selected' : '' }}>Islam
                                </option>
                                <option value="protestan"
                                    {{ $edit->patient->religion == 'protestan' ? 'selected' : '' }}>Protestan</option>
                                <option value="katolik" {{ $edit->patient->religion == 'katolik' ? 'selected' : '' }}>
                                    Katolik</option>
                                <option value="hindu" {{ $edit->patient->religion == 'hindu' ? 'selected' : '' }}>Hindu
                                </option>
                                <option value="buddha" {{ $edit->patient->religion == 'buddha' ? 'selected' : '' }}>
                                    Buddha</option>
                                <option value="khonghucu"
                                    {{ $edit->patient->religion == 'khonghucu' ? 'selected' : '' }}>Khonghucu</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Pekerjaan</label>
                        <div class="col-lg-8">
                            <select class="form-control" name="pekerjaan">
                                <option value="">Pilih Pekerjaan</option>
                                @foreach ($pekerjaan as $item)
                                    <option value="{{ $item->id_pekerjaan }}"
                                        {{ $edit->patient->pekerjaan == null ? '' : ($item->pkj_nama == $edit->patient->pekerjaan->pkj_nama ? 'selected' : '') }}>
                                        {{ $item->pkj_nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Pendidikan Terakhir</label>
                        <div class="col-lg-8">
                            <select class="form-control" name="study">
                                <option value="">Pilih Pendidikan Terhakhir</option>
                                <option value="sd" {{ $edit->patient->study == 'sd' ? 'selected' : '' }}>SD
                                </option>
                                <option value="smp" {{ $edit->patient->study == 'smp' ? 'selected' : '' }}>SMP
                                </option>
                                <option value="sma/smk" {{ $edit->patient->study == 'sma/smk' ? 'selected' : '' }}>
                                    SMA/SMK</option>
                                <option value="diploma" {{ $edit->patient->study == 'diploma' ? 'selected' : '' }}>
                                    Diploma</option>
                                <option value="sarjana" {{ $edit->patient->study == 'sarjana' ? 'selected' : '' }}>
                                    Sarjana</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Tempat Menempuh Pendidikan</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill" name="place_to_study" type="text"
                                value="{{ $edit->patient->place_to_study }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Tanggal Lahir</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_datepicker_1_modal" name="birthday"
                                value="{{ Main::format_date($edit->patient->birthday) }}" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Provinsi</label>
                        <div class="col-lg-8">
                            <select class="form-control m-input m-input--pill m-select2" name="id_province"
                                style="width: 100%">
                                <option value="">Pilih Provinsi</option>
                                @foreach ($province as $row)
                                    <option value="{{ $row->id_province }}"
                                        {{ $row->id_province == $edit->patient->id_province ? 'selected' : '' }}>
                                        {{ $row->province_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Kabupaten</label>
                        <div class="col-lg-8">
                            <select class="form-control m-input m-select2" name="id_city">
                                <option value="">Pilih Kabupaten</option>
                                @foreach ($city as $row)
                                    <option value="{{ $row->id_city }}"
                                        {{ $row->id_city == $edit->patient->id_city ? 'selected' : '' }}>
                                        {{ $row->city_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Kecamatan</label>
                        <div class="col-lg-8">
                            <select class="form-control m-input m-select2" name="id_subdistrict">
                                <option value="">Pilih Kecamatan</option>
                                @foreach ($subdistrict as $row)
                                    <option value="{{ $row->id_subdistrict }}"
                                        {{ $row->id_subdistrict == $edit->patient->id_subdistrict ? 'selected' : '' }}>
                                        {{ $row->subdistrict_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Alamat Detail</label>
                        <div class="col-lg-8">
                            <textarea class="form-control m-input m-input--pill" name="address" placeholder="Jln. Gatsu Barat, No 3 Desa Gatsu">{{ $edit->patient->address }}</textarea>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Berat Badan</label>
                        <div class="col-lg-2">
                            <input class="form-control m-input m-touchspin m-input--pill" name="weight"
                                id="m_touchspin_4" value="{{ $edit->patient->weight }}" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Jadwal Kalender</label>
                        <div class="col-lg-8">
                            {{--                            <button type="button" class="btn btn-primary m-btn--pill" data-toggle="modal" --}}
                            {{--                                    data-target="#modal-appointment-schedule"> --}}
                            {{--                                <i class="la la-eye"></i> Lihat Jadwal --}}
                            {{--                            </button> --}}
                            <a href="{{ route('scheduleCalendarPage') }}" class="btn btn-primary m-btn--pill"
                                target="_blank">
                                <i class="la la-eye"></i> Lihat Jadwal
                            </a>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Tanggal Appointment</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_datepicker_1_modal"
                                name="appointment_date" autocomplete="off" type="text"
                                value="{{ Main::format_date($edit->appointment_time) }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Jam Appointment</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_timepicker_1_modal"
                                name="appointment_hours" autocomplete="off" type="text"
                                value="{{ Main::format_time_db($edit->appointment_time) }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">No HP/WhatsApp</label>
                        <div class="col-lg-8">
                            <div class="row w-100">
                                <select class="form-control col-3" name="call_code">

                                    @foreach ($call_codes as $call_code)
                                        <option value="{{ $call_code->ccc_call_code }}"
                                            {{ $call_code_select == $call_code->id_country_call_code ? 'selected' : '' }}>
                                            {{ $call_code->ccc_country_name }} (+{{ $call_code->ccc_call_code }})

                                        </option>
                                    @endforeach

                                </select>
                                <input value="{{ $edit->phone_number }}"
                                    class="form-control m-input m-input--pill col-9" name="phone_1" type="text">
                            </div>
                            <span class="m-form__help">
                                Contoh Pengisian Nomer Telepon: 81934364063
                            </span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">No HP Lainnya</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill" name="phone_2" type="text"
                                value="{{ $edit->patient->phone_2 }}">
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Perbarui Data</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
</form>
