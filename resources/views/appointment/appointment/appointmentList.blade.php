@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-touchspin.js') }}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js') }}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/select2.js') }}" type="text/javascript">
    </script>
@endsection

@section('body')
    @include('appointment.appointment.appointmentCreate')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet ">
                <form method="get" class="m-form m-form--fit m-form--label-align-right form-dashboard-filter">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-2 col-sm-12">Rentang Tanggal Appointment</label>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <input type='text' class="form-control m_datepicker_1_modal" readonly
                                    value="{{ $date_from }}" placeholder="Select time" name="date_from" />
                            </div>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <input type='text' class="form-control m_datepicker_1_modal" readonly
                                    value="{{ $date_to }}" placeholder="Select time" name="date_to" />
                            </div>
                            <label class="col-form-label col-lg-1 col-sm-12 font-weight-bold">Nama Pasien</label>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <input type='text' name="name" value="{{ $name }}" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <button type="submit" class="btn btn-accent m-btn--pill btn-sm">
                            <i class="la la-search"></i> Filter Data
                        </button>
                    </div>
                </form>
            </div>

            <div class="m-portlet akses-list">
                <div class="m-portlet__body">
                    <input type="text" class="input-hide" value="{{ route('appointmentMemberNew') }}"
                        id="appointment-link">
                    <div class="pull-right text-center">
                        <button
                            class="akses-create_admin schedule-admin btn btn-sm btn-success m-btn m-btn--pill m-btn--air"
                            route="{{ route('appointmentSchedule') . '?from=' . $date_from . '&to=' . $date_to }}"> <i
                                class="la la-plane"></i> Kirim Email Schedule
                        </button>
                        &nbsp;
                        <a class="akses-create_admin btn btn-sm btn-danger m-btn m-btn--pill m-btn--air"
                            href="{{ route('appointmentDownload', 'xls') . '?from=' . $date_from . '&to=' . $date_to }}">
                            <i class="la la-file"></i> Download Excel
                        </a>
                        &nbsp;
                        <a type="button" class="akses-create_admin btn btn-sm btn-danger m-btn m-btn--pill m-btn--air"
                            href="{{ route('appointmentDownload', 'pdf') . '?from=' . $date_from . '&to=' . $date_to }}">
                            <i class="la la-download"></i> Download Pdf
                        </a>
                        &nbsp;
                        <button type="button"
                            class="akses-create_admin btn btn-sm btn-success m-btn m-btn--pill m-btn--air"
                            data-toggle="modal" data-target="#modal-appointment-create">
                            <i class="la la-plus"></i> Appointment Baru
                        </button>
                        &nbsp;
                        <a href="{{ route('appointmentMemberNew') }}" target="_blank"
                            class="akses-create_member btn btn-sm btn-primary m-btn m-btn--pill m-btn--air">
                            <i class="la la-eye"></i> Lihat Appointment Form
                        </a>
                        &nbsp;
                        <a href="#"
                            class="akses-create_member btn btn-sm btn-info m-btn m-btn--pill m-btn--air btn-appointment-copy-link"
                            data-link="{{ route('appointmentMemberNew') }}">
                            <i class="la la-copy"></i> Copy Link Appointment Form
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <hr />
                    <div class="table-responsive">
                        <table class="akses-list table table-bordered datatable-new"
                            data-url="{{ route('appointmentDataTable') }}" data-date-from="{{ $date_from }}"
                            data-date-to="{{ $date_to }}" data-name="{{ $name }}"
                            data-column="{{ json_encode($datatable_column) }}">
                            <thead>
                                <tr>
                                    <th width="20">No</th>
                                    <th class="no-sort">Keperluan</th>
                                    <th class="no-sort">Nama</th>
                                    <th class="no-sort">Berat Badan</th>
                                    <th class="no-sort">Umur</th>
                                    <th class="no-sort">Waktu Appointment</th>
                                    <th class="no-sort">Lokasi</th>
                                    <th class="no-sort">Reminder</th>
                                    <th class="no-sort" width="60">Menu</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
