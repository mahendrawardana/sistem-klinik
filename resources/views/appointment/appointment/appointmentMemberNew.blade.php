<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>
        Appointment Rumah Sunat Bali
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/loading.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="{{ asset('images/app-logo.png') }}" />
</head>

<body
    class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-aside-left--minimize m-brand--minimize m-footer--push m-aside--offcanvas-default">
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">

        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop">

            <div class="m-grid__item m-grid__item--fluid m-wrapper">

                <div class="m-content">
                    <!--begin::Portlet-->
                    <div class="m-portlet">
                        <div class="m-portlet__body m-portlet__body--no-padding">
                            <div class="m-pricing-table-2">
                                <div class="m-pricing-table-2__head">
                                    <div class="m-pricing-table-2__title m--font-light">
                                        <img src="{{ asset('images/logo-rumah-sunat-bali-login.png') }}">
                                        <h1>
                                            Appointment {{ env('BUSINESS_NAME') }}
                                        </h1>
                                    </div>

                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="m-pricing-table_content1" aria-expanded="true">
                                        <div class="m-pricing-table-2__content">
                                            <div class="m-pricing-table-2__container">
                                                <div class=" row">
                                                    <div
                                                        class="m-pricing-table-2__item m-pricing-table-2__items col-lg-6 offset-lg-3">
                                                        <form action="{{ route('appointmentMemberNewInsert') }}"
                                                            method="post" data-alert-show="true"
                                                            data-alert-field-message="true"
                                                            data-alert-show-success-status="true"
                                                            data-alert-show-success-title="Appointment Terkirim"
                                                            data-alert-show-success-message="Berhasil mengirim appointment untuk ditindak lanjuti"
                                                            class="m-form m-form--fit m-form--label-align-right form-send">
                                                            {{ csrf_field() }}

                                                            <input type="hidden" name="via" value="user">

                                                            <div class="m-portlet__body">
                                                                <div class="form-group m-form__group m--margin-top-10">
                                                                    <div class="alert m-alert m-alert--default"
                                                                        role="alert">
                                                                        Mohon untuk mengisi form dibawah untuk melakukan
                                                                        perjanjian konsultasi di
                                                                        {{ env('BUSINESS_NAME') }}<br />
                                                                        <span class="font-italic">(Please fill out the
                                                                            form below to do
                                                                            consulting agreement in)</span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <div class="col-12 font-italic">
                                                                        <span class="required-label">*</span>)
                                                                        diperlukan<br />
                                                                        <span class="required-label">*</span>)
                                                                        required<br />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <label for="exampleInputEmail1" class="required">
                                                                        Keperluan
                                                                    </label>
                                                                    <br />
                                                                    <label for="exampleInputEmail1" class="required">
                                                                        <span class="font-italic">(Necessities)</span>
                                                                    </label>
                                                                    <div class="m-radio-inline">
                                                                        <label class="m-radio">
                                                                            <input type="radio" name="need_type"
                                                                                value="consult" checked> Konsultasi
                                                                            <i>(Consultation)</i>
                                                                            <span></span>
                                                                        </label>
                                                                        <label class="m-radio">
                                                                            <input type="radio" name="need_type"
                                                                                value="action"> Tindakan <i>(Action)</i>
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <label for="exampleInputEmail1" class="required">
                                                                        Lokasi Klinik Tujuan
                                                                    </label><br />
                                                                    <label for="exampleInputEmail1" class="required">
                                                                        <span class="font-italic">(Destination Clinic
                                                                            Location)</span>
                                                                    </label>
                                                                    <div class="m-radio-inline">
                                                                        <label class="m-radio">
                                                                            <input type="radio" name="location"
                                                                                value="denpasar" checked> Denpasar
                                                                            <span></span>
                                                                        </label>
                                                                        <label class="m-radio">
                                                                            <input type="radio" name="location"
                                                                                value="nusa_dua"> Nusa Dua
                                                                            <span></span>
                                                                        </label>
                                                                        <label class="m-radio">
                                                                            <input type="radio" name="location"
                                                                                value="gianyar"> Gianyar
                                                                            <span></span>
                                                                        </label>
                                                                        <label class="m-radio">
                                                                            <input type="radio" name="location"
                                                                                value="home_service"> Home Service
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <label for="exampleInputEmail1" class="required">
                                                                        Nama
                                                                    </label><br />
                                                                    <label for="exampleInputEmail1"
                                                                        class="required font-italic">
                                                                        (Name)
                                                                    </label>
                                                                    <input class="form-control m-input m-input--pill"
                                                                        name="name" type="text">
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <label for="exampleInputEmail1" class="required">
                                                                        Agama
                                                                    </label><br />
                                                                    <label for="exampleInputEmail1"
                                                                        class="required font-italic">
                                                                        (Religion)
                                                                    </label>
                                                                    <select class="form-control" name="religion">
                                                                        <option value="islam">Islam</option>
                                                                        <option value="protestan">Protestan</option>
                                                                        <option value="katolik">Katolik</option>
                                                                        <option value="hindu">Hindu</option>
                                                                        <option value="buddha">Buddha</option>
                                                                        <option value="khonghucu">Khonghucu</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group m-form__group ">
                                                                    <label class="required">Pekerjaan (Job)</label>
                                                                    <select class="form-control  m-select2"
                                                                        name="pekerjaan">
                                                                        <option value="">Pilih Pekerjaan</option>
                                                                        @foreach ($pekerjaan as $item)
                                                                            <option value="{{ $item->id_pekerjaan }}">
                                                                                {{ $item->pkj_nama }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <label class="required">Pendidikan Terakhir</label>
                                                                    <label class="required font-italic">(Study)</label>
                                                                    <select class="form-control" name="study">
                                                                        <option value="">Pilih Pendidikan
                                                                            Terhakhir</option>
                                                                        <option value="sd">SD</option>
                                                                        <option value="smp">SMP</option>
                                                                        <option value="sma/smk">SMA/SMK</option>
                                                                        <option value="diploma">Diploma</option>
                                                                        <option value="sarjana">Sarjana</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <label class="required">Tempat Menempuh
                                                                        Pendidikan</label>
                                                                    <label class="required font-italic">(Place to
                                                                        Study)</label>
                                                                    <input class="form-control m-input m-input--pill"
                                                                        name="place_to_study" type="text">
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <label for="exampleInputEmail1" class="required">
                                                                        Tanggal Lahir
                                                                    </label><br />
                                                                    <label for="exampleInputEmail1"
                                                                        class="required font-italic">
                                                                        (Birthdate)
                                                                    </label>
                                                                    <input
                                                                        class="form-control m-input m-input--pill m_datepicker_1_modal"
                                                                        name="birthday" autocomplete="off"
                                                                        type="text">
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <label for="exampleInputEmail1" class="required">
                                                                        Provinsi
                                                                    </label><br />
                                                                    <label for="exampleInputEmail1"
                                                                        class="required font-italic">
                                                                        (Province)
                                                                    </label>
                                                                    <select
                                                                        class="form-control m-input m-input--pill m-select2"
                                                                        id="m_select2_1" name="id_province"
                                                                        data-placeholder="Pilih Provinsi">
                                                                        <option value="">Pilih Provinsi (Select
                                                                            Province)</option>
                                                                        @foreach ($province as $row)
                                                                            <option value="{{ $row->id_province }}">
                                                                                {{ $row->province_name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <label for="exampleInputEmail1" class="required">
                                                                        Kabupaten
                                                                    </label><br />
                                                                    <label for="exampleInputEmail1"
                                                                        class="required font-italic">
                                                                        (District)
                                                                    </label>
                                                                    <select class="form-control m-input"
                                                                        name="id_city">
                                                                        <option value="">Pilih Kabupaten (Select
                                                                            District)</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <label for="exampleInputEmail1" class="required">
                                                                        Kecamatan
                                                                    </label><br />
                                                                    <label for="exampleInputEmail1"
                                                                        class="required font-italic">
                                                                        (Sub District)
                                                                    </label>
                                                                    <select class="form-control m-input"
                                                                        name="id_subdistrict">
                                                                        <option value="">Pilih Kecamatan
                                                                            (Subdistrict Select)</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <label for="exampleInputEmail1" class="required">
                                                                        Alamat Detail
                                                                    </label><br />
                                                                    <label for="exampleInputEmail1"
                                                                        class="required font-italic">
                                                                        (Detail Address)
                                                                    </label>
                                                                    <textarea class="form-control m-input m-input--pill" name="address" placeholder="Jln. Gatsu Barat, No 3 Desa Gatsu"></textarea>
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <label for="exampleInputEmail1" class="required">
                                                                        Berat Badan
                                                                    </label><br />
                                                                    <label for="exampleInputEmail1"
                                                                        class="required font-italic">
                                                                        (Body Weight)
                                                                    </label>
                                                                    <input class="form-control m-input m-input--pill"
                                                                        name="weight" id="m_touchspin_4"
                                                                        value="55" type="text">
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <label>Tanggal Appointment</label><br />
                                                                    <label for="exampleInputEmail1"
                                                                        class="required font-italic">
                                                                        (Appointment Date)
                                                                    </label>
                                                                    <input
                                                                        class="form-control m-input m-input--pill m_datepicker_1_modal"
                                                                        name="appointment_date" autocomplete="off"
                                                                        type="text" value="{{ date('d-m-Y') }}">
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <label>Jam Appointment</label><br />
                                                                    <label for="exampleInputEmail1"
                                                                        class="required font-italic">
                                                                        (Appointment Time)
                                                                    </label>
                                                                    <input
                                                                        class="form-control m-input m-input--pill m_timepicker_1_modal"
                                                                        name="appointment_hours" autocomplete="off"
                                                                        type="text">
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <label for="exampleInputEmail1" class="required">
                                                                        No HP/WhatsApp
                                                                    </label><br />
                                                                    <label for="exampleInputEmail1"
                                                                        class="required font-italic">
                                                                        (Phone/WhatsApp Number)
                                                                    </label>
                                                                    <div class="row w-100">
                                                                        <select
                                                                            class="form-control m-input m-input--pill col-5 m-select2"
                                                                            name="call_code">
                                                                            <option value="62">
                                                                                {{ 'Indonesia (+62)' }}</option>
                                                                            <optgroup label="Other Countries">
                                                                                @foreach ($call_codes as $call_code)
                                                                                    <option
                                                                                        value="{{ $call_code->ccc_call_code }}">
                                                                                        {{ $call_code->ccc_country_name }}
                                                                                        (
                                                                                        {{ "+$call_code->ccc_call_code" }}
                                                                                        )</option>
                                                                                @endforeach
                                                                            </optgroup>
                                                                        </select>
                                                                        <input
                                                                            class="form-control m-input m-input--pill col-7"
                                                                            name="phone_1" type="text">
                                                                    </div>
                                                                    <span class="m-form__help">
                                                                        Contoh Pengisian Nomer Telepon: 81934364063
                                                                    </span>
                                                                    <br />
                                                                    <span class="m-form__help font-italic">
                                                                        (Example: 81934364063)
                                                                    </span>
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <label for="exampleInputEmail1">
                                                                        No HP Lainnya
                                                                    </label><br />
                                                                    <label for="exampleInputEmail1"
                                                                        class="required font-italic">
                                                                        (Other Phone Number)
                                                                    </label>
                                                                    <input
                                                                        class="form-control m-input m-input--pill col-9"
                                                                        name="phone_2" type="text">
                                                                </div>


                                                            </div>
                                                            <div class="m-portlet__foot m-portlet__foot--fit">
                                                                <div class="m-form__actions text-center">
                                                                    <button type="submit"
                                                                        class="btn btn-success m-btn--pill">
                                                                        Buat Appointment (Create Appointment)
                                                                    </button>
                                                                    {{--                                                                <a href="{{ \app\Helpers\Main::$website_official }}" --}}
                                                                    {{--                                                                   target="_blank" class="btn btn-info m-btn--pill"> --}}
                                                                    {{--                                                                    Lihat Website Official --}}
                                                                    {{--                                                                </a> --}}
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>

    </div>

    <span id="base-value" class="hidden" data-route-city-list="{{ route('cityList') }}"
        data-route-subdistrict-list="{{ route('subdistrictList') }}" data-csrf-token="{{ csrf_token() }}"></span>


    <div class='container-loading hidden'>
        <div class='loader'>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--text'></div>
            <div class='loader--desc'></div>
        </div>
    </div>


    <script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js') }}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-touchspin.js') }}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/select2.js') }}" type="text/javascript">
    </script>
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
    <!--end::Base Scripts -->
</body>
<!-- end::Body -->

</html>
