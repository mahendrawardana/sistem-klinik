@php
    $location = '';
        if($row->need_type == 'consult') {
            $location = \app\Models\mConsult::where('id_consult', $row->id_consult)->value('consult_location');
        } else if($row->need_type == 'action') {
            $location = \app\Models\mAction::where('id_action', $row->id_action)->value('action_location');
        } else if($row->need_type == 'control') {
            $location = \app\Models\mControl::where('id_control', $row->id_control)->value('control_location');
        }
@endphp

{{ Main::greating() }}<br />
<br />
Ada appointment calon pasien baru yang sudah melakukan registrasi,<Br />
Mohon untuk di check di sistem <a href="app.hubpasien.com">Hub Pasien</a>.<br />
Berikut Detail informasi pasien tersebut: <br />
<br />
Keperluan : {{ Main::need_type_label($row->need_type) }}<Br />
Nama : {{ $row->patient->name }}<Br />
Umur : {{ Main::format_age($row->patient->birthday) }}<Br />
Tanggal Lahir : {{ Main::format_date_label($row->patient->birthday) }}<Br />
Provinsi : {{ $row->patient->province->province_name }}<Br />
Kabupaten : {{ $row->patient->city->city_name }}<Br />
Kecamatan : {{ $row->patient->subdistrict->subdistrict_name }}<Br />
Alamat : {{ $row->patient->address }}<Br />
Berat Badang : {{ $row->patient->weight }} KG<Br />
Waktu Appointment : {{ Main::format_datetime($row->appointment_time) }}<Br />
Lokasi : {{ Main::locationLabelView($location) }}<Br />
No HP/WhatsApp : {{ $row->patient->phone_1 }}<Br />
No HP Lainnya : {{ $row->patient->phone_2 }}<Br />
<Br />
Hub Pasien<Br />
Sistem CRM dan Manajemen Pasien<Br />
<a href="www.hubpasien.com">www.hubpasien.com</a><Br />
<a href="www.mahendrawardana.com">www.mahendrawardana.com</a><Br />