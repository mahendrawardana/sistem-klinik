<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <style type="text/css">
        @page {
            margin: 6px 15px;
        }

        body {
            font-size: 11px;
            margin: 2px 15px;
        }

        h1 {
            font-size: 20px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h2 {
            font-size: 16px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h3 {
            font-size: 18px;
            font-weight: bold;
            text-decoration: underline;
        }

        .div-2 {
            width: 50%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 {
            width: 33.3%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 tr td {
            vertical-align: top;
            padding-top: 0;
        }

        .label-1 {
            margin-top: -10px;
            margin-bottom: -14px;
            padding-left: 30%;
        }

        .label-1 label {
            padding-top: 12px;
        }

        .label-2 {
            margin-top: -5px;
            margin-bottom: -6px;
            padding-left: 30%;
        }

        .label-2 label {
            padding-top: 12px;
        }

        .table th,
        .table td {
            padding: 2px 4px;
        }

        .table th {
            text-align: center;
        }

        .table th.number,
        .table td.number {
            text-align: right;
        }

        .bold {
            font-weight: bold;
        }

        .sub-1 {
            padding-left: 20px;
        }

        .sub-2 {
            padding-left: 40px;
        }
    </style>
    <?php
    $width_label = '64';
    ?>

</head>

<body>
    <div class="text-center">
        <img src="{{ asset('images/logo-rumah-sunat-bali-login.png') }}" class="m-4">
    </div>
    <table width="100%" class="table">
        <caption>
            {{ 'Data Appointment ' . Main::format_date_label($date_from) . ' - ' . Main::format_date_label($date_to) }}
        </caption>
        <thead>
            <tr>
                <th>No</th>
                <th>Need Type</th>
                <th>Name</th>
                <th>Weight</th>
                <th>Age</th>
                <th>Appointment Time</th>
                <th>Location</th>
                <th>Reminder Status</th>
            </tr>
        </thead>
        <tbody>
            @if ($appointment->count() == 0)
                <tr>
                    <td colspan="7" align="center">Tidak ada data</td>
                </tr>
            @endif
            @foreach ($appointment as $i => $row)
                <tr>
                    <td>{{ $i + 1 }}</td>
                    <td>{!! Main::need_type_span($row->need_type, $row->control_step) !!}</td>
                    <td>{{ $row->name }}</td>
                    <td>{{ $row->weight }} KG</td>
                    <td>{{ Main::format_age($row->birtdday) }}</td>
                    <td>{!! Main::format_datetime($row->appointment_time) !!}</td>
                    <td>{{ Main::locationLabelView($row->location) }}</td>
                    <td>{!! $row->whatsapp_send == 'yes'
                        ? Main::appointment_status($row->appointment_status)
                        : '<span class="m-badge m-badge--default m-badge--wide">Tidak Direminder</span>' !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <script>
        print()
    </script>
</body>
