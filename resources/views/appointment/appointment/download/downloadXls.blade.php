@php
    
    header('Content-Type:   application/vnd.ms-excel; charset=utf-8');
    header('Content-Disposition: attachment; filename=data_appointment_dari_' . $date_from . '_sampai_' . $date_to . '.xls'); //File name extension was wrong
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Cache-Control: private', false);
    
@endphp

<style>
    .str {
        mso-number-format: \@;
    }
</style>

<table width="100%" border="1">
    <caption>{{ 'Data Pasien ' . Main::format_date_label($date_from) . ' - ' . Main::format_date_label($date_to) }}
    </caption>
    <thead>
        <tr>
            <th>No</th>
            <th>Need Type</th>
            <th>Name</th>
            <th>Weight</th>
            <th>Age</th>
            <th>Appointment Time</th>
            <th>Location</th>
            <th>Reminder Status</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($appointment as $i => $row)
            <tr>
                <th>{{ $i + 1 }}</th>
                <th>{!! Main::need_type_span($row->need_type, $row->control_step) !!}</th>
                <th>{{ $row->name }}</th>
                <th>{{ $row->weight }} KG</th>
                <th>{{ Main::format_age($row->birthday) }}</th>
                <th>{{ Main::format_datetime($row->appointment_time) }}</th>
                <th>{{ Main::locationLabelView($row->location) }}</th>
                <th>{!! $row->whatsapp_send == 'yes'
                    ? Main::appointment_status($row->appointment_status)
                    : '<span class="m-badge m-badge--default m-badge--wide">Tidak Direminder</span>' !!}
                </th>
            </tr>
        @endforeach
    </tbody>
</table>
