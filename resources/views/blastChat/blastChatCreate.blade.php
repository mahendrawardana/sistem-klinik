<form action="{{ route('blastChatInsert') }}" method="post" class="m-form form-send" autocomplete="off">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Blast Chat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Judul Blast Chat </label>
                            <input type="text" class="form-control m-input" name="nama_blast_chat" autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Variable </label>
                            <div class="input-group mb-2 variable w-100" keys="variable">
                                <input type="text" name="variable[]" class="form-control" placeholder="Masukan Nama Variable">
                            </div>
                        </div>

                        <span class="input-group-btn mt-3 mb-2">
                            <button class="btn btn-primary" onclick="addVariable()" type="button">
                                <i class="la la-plus"> </i> Tambah Variabel
                            </button>
                        </span>

                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Waktu Kirim </label>
                            <div class="input-group">
                                <input type="text" name="waktu_kirim" class="w-100 form-control m_datetimepicker_1_modal" readonly placeholder="--\--\---- --:--:--">
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Template Chat </label>
                            <div class="alert alert-success" role="alert">
                                <strong>
                                    Catatan : <br/>
                                </strong>
                                Cara menggunakan variable pada message adalah dengan mengisikan tanda bracket diantara variable, contohnya seperti ini {nama} {nohp} {alamat} dan lain sebagainya.
                            </div>
                            <textarea class="form-control m-input" name="template" rows="14"></textarea>
                        </div>

                        <div class="form-group m-form__group">
                            <label class="form-control-label">Catatan </label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="catatan" placeholder="Masukan catatan">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">Simpan</button>
                    <button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>
