<div class="modal" id="modal-general" role="dialog"
     aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content detail-info">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    <i class="la la-info"></i> Detail BlastChat
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Judul</label>
                    <div class="col-lg-8">
                        {{ $blast_chat->blc_title }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Variable</label>
                    <div class="col-lg-8">
                      @php $variables = json_decode($blast_chat->blc_variable_key) @endphp
                      @if(count($variables) == 0)
                        <span>Tidak ada variable</span>
                      @else 
                        <table class="table">
                            <tr>
                                <th>No.</th>
                                <th>Value</th>
                            </tr>
                            @foreach( $variables as $key => $variable ) 
                                <tr>
                                    <td col="10px"> {{ ++$key }} </td>
                                    <td><span style="border-radius:4px; width: max-content"> {{$variable}} </span></td>
                                    </tr>
                            @endforeach
                        </table>
                      @endif 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Waktu Kirim</label>
                    <div class="col-lg-8">
                        {{ $blast_chat->blc_send_time }}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Template Chat</label>
                    <textarea readonly class="col-lg-8 form-control m-input" rows="10">{!! $blast_chat->blc_chat_template !!}</textarea>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Catatan</label>
                    <div class="col-lg-8">
                        {{ $blast_chat->blc_notes }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Total Penerima</label>
                    <div class="col-lg-2">
                        {{ $total_recepient }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Total Terkirim</label>
                    <div class="col-lg-8">
                        {{ $total_send }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Total Gagal</label>
                    <div class="col-lg-8">
                        {{ $total_fail }}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

