<div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Blast Chat</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <form action="{{ route('blastChatUpdate',['id_blast_chat' => $id_blast_chat] ) }}" method="post" class="m-form form-send" autocomplete="off">
                  {{ csrf_field() }}

                  <div class="modal-body">
                      <div class="m-portlet__body">
                          <div class="form-group m-form__group">
                              <label class="form-control-label required">Judul Blast Chat </label>
                              <input type="text" class="form-control m-input" name="nama_blast_chat" value="{{$blast_chat->blc_title}}"
                                     autofocus>
                          </div>
                           <div class="form-group m-form__group">
                              <label class="form-control-label">Variable </label>

                              <div class="input-group mb-2 variable w-100" keys="variable">
                                  <input type="text" value="{{ $first_variable }}" name="variable[]" class="form-control" placeholder="Masukan Nama Variable">
                              </div> 
                              @foreach($variables as $variable)
                              <div class="input-group mb-2 variable" id="{{strtotime(date('Y-m-d H:i:s'))}}"> 
                                <input type="text" name="variable[]" class="form-control" value="{{ $variable }}" placeholder="Masukan Nama Variable">
                                <span class="input-group-btn remove-button" onclick="removeVariable(this)">
                                  <button class="btn btn-danger" type="button">
                                    <i class="la la-times">  </i> 
                                  </button>
                                </span>
                              </div>
                              @endforeach

                          </div>

                          <span class="input-group-btn mt-3 mb-2">
                              <button class="btn btn-primary" onclick="addVariable(this)" type="button">
                                  <i class="la la-plus">  </i> Tambah Variabel
                              </button>
                          </span>

                          <div class="form-group m-form__group">
                              <label class="form-control-label required">Waktu Kirim </label>
                              <div class="input-group">
                                  <input type="text" name="waktu_kirim" value="{{ $blast_chat->blc_send_time }}" class="w-100 form-control m_datetimepicker_1_modal" readonly placeholder="--\--\---- --:--:--">
                              </div>
                          </div>
                          <div class="form-group m-form__group">
                              <label class="form-control-label required">Template Chat  </label>
                                <div class="alert alert-success" role="alert">
                                    <strong>
                                        Catatan : </br>
                                    </strong>
                                    Cara menggunakan variable pada message adalah dengan mengisikan tanda bracket diantara variable, contohnya seperti ini {nama} {nohp} {alamat} dan lain sebagainya.
                                </div>
                              <textarea class="form-control m-input" name="template" rows="14"> {!! $blast_chat->blc_chat_template !!} </textarea>
                          </div>

                          <div class="form-group m-form__group">
                              <label class="form-control-label">Catatan </label>
                              <div class="input-group">
                                  <input type="text" class="form-control" value="{{$blast_chat->blc_notes}}" name="catatan" placeholder="Masukan catatan">
                              </div>
                          </div>

                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="submit" class="btn btn-simpan btn-info m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">Update</button>
                      <button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air" data-dismiss="modal">Batal</button>
                  </div>
              </form>
          </div>
    </div>
</div>

