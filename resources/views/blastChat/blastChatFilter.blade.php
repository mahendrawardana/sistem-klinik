<div class="m-portlet">
    <form method="get" class="m-form m-form--fit m-form--label-align-right form-dashboard-filter">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row text-center">
                <label class="col-form-label col-lg-2 col-sm-12">Rentang Tanggal Blast Chat</label>
                <div class="col-lg-2 col-md-9 col-sm-12">
                    <input type='text'
                            class="form-control m_datetimepicker_1_modal"
                            readonly
                            value="{{ $date_from }}"
                            placeholder="Select time"
                            name="date_from"/>
                </div>
                <div class="col-lg-2 col-md-9 col-sm-12">
                    <input type='text'
                            class="form-control m_datetimepicker_1_modal"
                            readonly
                            value="{{ $date_to }}"
                            placeholder="Select time"
                            name="date_to"/>
                </div>
                <label class="col-form-label col-lg-1 col-sm-12 font-weight-bold">Kata Kunci</label>
                <div class="col-lg-2 col-md-9 col-sm-12">
                    <input type='text'
                            name="keyword"
                            value="{{ $keyword }}"
                            class="form-control"/>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot text-center">
            <button type="submit" class="btn btn-accent m-btn--pill btn-sm">
                <i class="la la-search"></i> Filter Data
            </button>
        </div>
    </form>
</div>