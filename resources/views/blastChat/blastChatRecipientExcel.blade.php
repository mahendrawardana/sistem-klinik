@php
    $blast_chat->blc_variable_key = json_decode($blast_chat->blc_variable_key);
    
@endphp
<style> .str{ mso-number-format:\@; } </style>
<table>
    <thead>
    <tr>
        <th>no</th>
        <th>No HP</th>
        @foreach ($blast_chat->blc_variable_key as $item)
            <th>{{ $item }}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @for($i=1; $i<=100; $i++)
        <tr>
            <td>{{ $i }}</td>
            <td class="str"></td>
            @foreach ($blast_chat->blc_variable_key as $item)            
                <td></td>
            @endforeach
        </tr>
    @endfor
    </tbody>
</table>