
    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Daftar Penerima Chat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <table class="table table-bordered datatable-new"
                               data-url="{{ route('patientDataTable') }}"
                               data-date-from="{{ $date_from }}"
                               data-date-to="{{ $date_to }}"
                               data-name="{{ $name }}"
                               data-column="{{ json_encode($datatable_column) }}">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Nama</th>
                                <th>Berat Badan</th>
                                <th>Umur</th>
                                <th>Alamat</th>
                                <th>No HP/WA</th>
                                <th>Tahap</th>
                                <th width="60">Menu</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                </div>
            </div>
        </div>
    </div>