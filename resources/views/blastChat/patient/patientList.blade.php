

 <div class="modal" id="modal-patient-list" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-xl" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Blast Chat</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <div class="table-responsive">
                     <table class="table table-bordered datatable-new" data-url="{{ route('patientDataTable') }}" data-date-from="{{ $date_from }}" data-date-to="{{ $date_to }}" data-pick-number="yes" data-column="{{ json_encode($datatable_column) }}">
                         <thead>
                             <tr>
                                 <th width="20">No</th>
                                 <th>Nama</th>
                                 <th>Berat Badan</th>
                                 <th>Umur</th>
                                 <th>Alamat</th>
                                 <th>No HP/WA</th>
                                 <th>Tahap</th>
                                 <th width="60">Menu</th>
                             </tr>
                         </thead>
                         <tbody>
                         </tbody>
                     </table>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air" data-dismiss="modal">Batal</button>
             </div>
         </div>
     </div>
 </div>
