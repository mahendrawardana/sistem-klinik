<script>
  variableContentChange();
  btn_modal_general();
</script>
  <form action="{{ route('blastChatRecipientUpdate',['id_recipient' => $id_recipient]) }}" method="post" class="m-form form-send" autocomplete="off">
    {{ csrf_field() }}
    <div class="modal" id="modal-edit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Blast Chat Recipient</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body">
            <div class="m-portlet__body">
              <div class="form-group m-form__group">
                <label class="form-control-label required">Nomor Whatsapp </label>
                <div class="input-group">
                  <select class="form-control" style="width: 25px" name="call_code" onchange="whatsappNumberChange()"  >                                       
                      @foreach($call_codes as $call_code)                            
                          <option {{ $call_code_select->id_country_call_code == $call_code->id_country_call_code ? 'selected' : '' }}  value="{{ $call_code->id_encrypt }}">{{"$call_code->ccc_country_name (+$call_code->ccc_call_code)"}}</option>
                      @endforeach                       
                  </select>
                  <input oninput="whatsappNumberChange()" type="number" class="form-control m-input" name="nomor_whatsapp" autofocus value="{{ $recipient->bcr_whatsapp_number }}">
              </div>              
                <input hidden name="id_patient" value="{{ $recipient->id_patient ? Main::encrypt($recipient->id_patient) : '' }}">
                <span class="input-group-btn">
                  <a data-route="{{ route('pickPatientNumber') }}" style="cursor:default" type="button" class="btn btn-info btn-modal-general"><i class="la la-search"></i> Cari Pasien</a>
                </span>

              </div>
              <div class="form-group m-form__group">
                <label class="form-control-label">Variable </label>
                @foreach($variables as $index => $variable)
                <div class="input-group mb-2 variable w-100" keys="variable">

                  <span class="input-group-btn" style="width: 100px">
                    <button style="cursor:default" type="button" class="w-100 btn btn-info disabled"> {{ $index }}</button>
                  </span>

                  <input varname="{{$index}}" type="text" value="{{$variable}}" name="variable_content[{{$index}}]" class="form-control variable-content" placeholder="Masukan Variable Konten">
                </div>
                @endforeach
              </div>

              <div class="form-group m-form__group">
                <label class="form-control-label required">Hasil isi Chat </label>
                <textarea readonly data-template="{{$blast_chat->blc_chat_template}}" class="form-control m-input" name="hasil_chat" rows="14">{{$recipient->bcr_message}}</textarea>
              </div>

              <div class="form-group m-form__group">
                <label class="form-control-label">Waktu Kirim </label>
                <div class="input-group">
                  {{ $send_time }}
                </div>
              </div>


            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-simpan btn-success m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">Simpan</button>
            <button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air" data-dismiss="modal">Batal</button>
          </div>
        </div>
      </div>
    </div>
  </form>

