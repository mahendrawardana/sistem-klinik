<form action="{{ route('blastChatRecipientImport',['id_blast_chat' => $id_blast_chat]) }}" method="post"
      class="m-form form-send" enctype="multipart/form-data" autocomplete="off">
    {{ csrf_field() }}
    <div class="modal" id="modal-import" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Blast Chat Recipient</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="m-portlet__body">
                        <a href="{{route('generateTemplateExcel',$id_blast_chat)}}"
                           class="akses-create btn btn-accent mb-5 m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create">
                            <span>
                                <i class="la la-download"></i>
                                <span>Download Contoh Excel</span>
                            </span>
                        </a>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Excel File </label>
                            <input type="file" class="form-control m-input" name="file_xls" autofocus>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit"
                            class="btn btn-simpan btn-success m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        Simpan
                    </button>
                    <button type="button"
                            class="btn btn-secondary m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                            data-dismiss="modal">Batal
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
