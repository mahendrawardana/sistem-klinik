@extends('../general/index')

@section('css')
<link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('js')
<script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-touchspin.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
@endsection

@section('body')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                    {{ $pageTitle }}
                </h3>
                {!! $breadcrumb !!}
            </div>
        </div>
    </div>
    <div class="m-content">
        @include('blastChat.recipient.recipientCreateModal')
        @include('blastChat.recipient.recipientRangkuman')
        @include('blastChat.recipient.recipientImportExcel')
        <div class="m-portlet m-portlet--mobile akses-list">
            <div class="m-portlet__body">
                <div class="d-flex justify-content-end w-100 mb-3">
                    <a href="#" class="akses-create btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create" data-toggle="modal" data-target="#modal-import">
                        <span>
                            <i class="la la-download"></i>
                            <span>Import Excel</span>
                        </span>
                    </a>
                    <a href="{{ route('blastChatList') }}" class="btn btn-success m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air mx-2">
                        <span>
                            <i class="la la-arrow-left"></i>
                            <span>Kembali</span>
                        </span>
                    </a>

                    <a href="#" class="akses-create btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create" data-toggle="modal" data-target="#modal-create">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Recipient</span>
                        </span>
                    </a>
                </div>
                <table class="akses-list datatable-new datatable table table-striped table-bordered table-hover table-checkable datatable-general" data-url="{{ route('blastChatRecipientDataTable') }}" data-id-blast-chat="{{ $id_blast_chat }}" data-column="{{ json_encode($datatable_column) }}">
                    <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Nomor Whatsapp</th>
                            <th>Variable Content</th>
                            <th>Message</th>
                            <th>Jenis Penerima</th>
                            <th>Waktu Kirim</th>
                            <th>Status Kirim</th>
                            <th width="100">Menu</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
@endsection
