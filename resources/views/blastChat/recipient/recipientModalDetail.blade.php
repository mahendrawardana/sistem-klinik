<div class="modal" id="modal-detail" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content detail-info">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    <i class="la la-info"></i> Detail Recipient
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Nama Pasien</label>
                    <div class="col-lg-8">
                        {{ $recipient->patient ? $recipient->patient->name : '-'  }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">variable Value</label>
                    <div class="col-lg-8">
                        @php $variables = json_decode($recipient->bcr_variable_value) ?? []; $no = 0;@endphp
                  
                        @if(count((array)$variables) == 0)
                            <span>Tidak ada variable</span>
                        @else 
                            <table class="table">
                                <tr>
                                    <th>No.</th>
                                    <th>Key</th>
                                    <th>Value</th>
                                </tr>
                                @foreach( $variables as $key => $variable ) 
                                    <tr>
                                        <td col="10px"> {{ ++$no }} </td>
                                        <td><span style="border-radius:4px; width: max-content"> {{$key}} </span></td>
                                        <td><span style="border-radius:4px; width: max-content"> {{$variable}} </span></td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Waktu Kirim</label>
                    <div class="col-lg-8">
                        {{ $recipient->bcr_send_time }}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Hasil Chat</label>
                    <textarea class="col-lg-8 form-control" readonly style="resize: none;" rows="{{ (count(explode(PHP_EOL,$recipient->bcr_message)) + 2) }}"  >{!! $recipient->bcr_message !!}</textarea>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Nomor Whatsapp</label>
                    <div class="col-lg-8">
                        +{{ $recipient->bcr_whatsapp_number }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Status Kirim</label>
                    <div class="col-lg-2">
                        {{ $recipient->bcr_send_status == 'not_yet' ? 'Belum Terkirim' : ( $recipient->bcr_send_status == 'stop' ?  'DiBerhentikan' : 'Sudah' ) }}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
