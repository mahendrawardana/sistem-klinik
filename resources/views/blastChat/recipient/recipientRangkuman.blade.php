<div class="m-portlet">
  <div class="m-portlet__body">
    <h4 class="mb-3">Rangkuman</h4>
    <table class="border-less">
      <tr>
        <td class="font-weight-bold" width="120px">
          Judul
        </td>
        <td width="240px">
          : {{$blast_chat->blc_title ?? '-'}}
        </td>
        <td class="font-weight-bold" width="80px">
          variable
        </td>
        <td width="240px">
          : {{ implode(', ',$variables) ?? '-' }}
        </td>
      </tr>

      <tr>
        <td class="font-weight-bold">
          waktu kirim
        </td>
        <td>
          : {{$send_time ?? '-'}}
        </td>
        <td class="font-weight-bold">
          catatan
        </td>
        <td>
          : {{ $blast_chat->notes ?? '-'}}
        </td>
      </tr>
    </table>
  </div>
</div>
