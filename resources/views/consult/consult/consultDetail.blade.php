<div class="modal" id="modal-general" role="dialog"
     aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">

            <ul class="nav nav-tabs" role="tablist" style="background-color: #e8e8e8">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tab-patient">
                        1. Detail Pasien
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-appointment">
                        2. Appointment
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-consult">
                        3. Konsultasi
                    </a>
                </li>
            </ul>
            <div class="modal-body">

                <div class="tab-content">
                    <div class="tab-pane active" id="tab-patient" role="tabpanel">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Nama</label>
                            <div class="col-lg-8">
                                {{ $row->patient->name }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Agama</label>
                            <div class="col-lg-8">
                                {{ ucwords($row->patient->religion) }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Pekerjaan</label>
                            <div class="col-lg-8">
                                {{ $row->patient->pekerjaan ? ucwords($row->patient->pekerjaan->pkj_nama) : '-' }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Pendidikan Terakhir</label>
                            <div class="col-lg-8 ">
                                {{ $row->patient->study ? ucwords($row->patient->study) : '-' }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Tempat Menempuh Pendidikan</label>
                            <div class="col-lg-8 ">
                                {{ $row->patient->place_to_study ? ucwords($row->patient->place_to_study) : '-' }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Umur</label>
                            <div class="col-lg-8">
                                {{ Main::format_age($row->patient->birthday) }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Tanggal Lahir</label>
                            <div class="col-lg-8">
                                {{ Main::format_date_label($row->patient->birthday) }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Provinsi</label>
                            <div class="col-lg-8">
                                {{ $row->patient->province->province_name }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Kabupaten</label>
                            <div class="col-lg-8">
                                {{ $row->patient->city->city_name }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Kecamatan</label>
                            <div class="col-lg-8">
                                {{ $row->patient->subdistrict->subdistrict_name }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Alamat</label>
                            <div class="col-lg-8">
                                {{ $row->patient->address }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Berat Badan</label>
                            <div class="col-lg-8">
                                {{ $row->patient->weight }} KG
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">No HP/WhatsApp</label>
                            <div class="col-lg-8">
                                {{ $row->patient->phone_1 }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">No HP Lainnya</label>
                            <div class="col-lg-8">
                                {{ $row->patient->phone_2 }}
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab-appointment" role="tabpanel">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Waktu Appointment</label>
                            <div class="col-lg-8">
                                {{ Main::format_datetime($row->appointment->appointment_time) }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Keperluan saat Appointment</label>
                            <div class="col-lg-8">
                                {{ Main::need_type_label($row->appointment->need_type) }}
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab-consult" role="tabpanel">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Waktu Konsultasi</label>
                            <div class="col-lg-8">
                                {{ Main::format_datetime($row->consult_time) }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Tempat Konsultasi</label>
                            <div class="col-lg-8">
                                {{ Main::locationLabelView($row->consult_location) }}
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
