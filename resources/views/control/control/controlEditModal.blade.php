<form action="{{ route('controlUpdate', ['id_control' => Main::encrypt($control->id_control)]) }}" method="post"
      class="form-send">

    {{ csrf_field() }}

    <div class="modal" id="modal-general" role="dialog"
         aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        <i class="la la-pencil"></i> Edit Kontrol
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Lokasi Klinik Tujuan</label>
                        <div class="col-lg-8">
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="control_location" value="panjer" {{ $control->control_location == 'panjer' ? 'checked':'' }}> Panjer
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="control_location" value="nusa_dua" {{ $control->control_location == 'nusa_dua' ? 'checked':'' }}> Nusa Dua
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="control_location" value="home_service" {{ $control->control_location == 'home_service' ? 'checked':'' }}> Home Service
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Kontrol ke</label>
                        <div class="col-lg-2">
                            <input class="form-control m-input m-input--pill" min="1" name="control_number" value="{{ $control->control_number }}" type="number">
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Jadwal Kalender</label>
                        <div class="col-lg-8">
                            {{--                            <button type="button" class="btn btn-primary m-btn--pill" data-toggle="modal"--}}
                            {{--                                    data-target="#modal-appointment-schedule">--}}
                            {{--                                <i class="la la-eye"></i> Lihat Jadwal--}}
                            {{--                            </button>--}}
                            <a href="{{ route('scheduleCalendarPage') }}" class="btn btn-primary m-btn--pill" target="_blank">
                                <i class="la la-eye"></i> Lihat Jadwal
                            </a>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Tanggal Kontrol</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_datepicker_1_modal"
                                   name="control_date" autocomplete="off" type="text" value="{{ Main::format_date($control->control_time) }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Jam Kontrol</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_timepicker_1_modal"
                                   name="control_hours" autocomplete="off" type="text" value="{{ Main::format_time_db($control->control_time) }}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Perbarui Data</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
</form>
