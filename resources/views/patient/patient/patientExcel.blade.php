@php

    header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
    header("Content-Disposition: attachment; filename=data_pasien_dari_".$date_from."_sampai_".$date_to.".xls");  //File name extension was wrong
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false);

@endphp

<style> .str{ mso-number-format:\@; } </style>

<table width="100%" border="1">
    <caption>{{ 'Data Pasien '.Main::format_date_label($date_from).' - '.Main::format_date_label($date_to) }}</caption>
    <thead>
    <tr>
        <th width="20">No</th>
        <th>Nama</th>
        <th>Agama</th>
        <th>Pekerjaan</th>
        <th>Pendidikan Terakhir</th>
        <th>Tempat Menempuh pendidikan</th>
        <th>Tanggal Lahir</th>
        <th>Umur</th>
        <th>Berat Badan</th>
        <th>Provinsi</th>
        <th>Kabupaten</th>
        <th>Kecamatan</th>
        <th>Alamat</th>
        <th>Telepon 1</th>
        <th>Telepon 2</th>
        <th>Status</th>
        <th>Tanggal Input Data</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data_list as $no => $row)
        <tr>
            <td align="center" class="str" width="50">{{ ++$no }}.</td>
            <td class="str">{{ $row->name }}</td>
            <td class="str">{{ $row->religion }}</td>
            <td class="str" >{{ $row->pekerjaan ? $row->pekerjaan->pkj_nama  : '-' }} </td>
            <td class="str" >{{ $row->study ?? '-' }} </td>
            <td class="str" >{{ $row->place_to_study ?? '-' }} </td>
            <td class="str">{{ Main::format_date_label($row->birthday) }}</td>
            <td class="str">{{ Main::format_age($row->birthday) }}</td>
            <td class="str">{{ $row->weight }} Kg.</td>
            <td class="str">{{ $row->province->province_name }}</td>
            <td class="str">{{ $row->city->city_name }}</td>
            <td class="str">{{ $row->subdistrict->subdistrict_name }}</td>
            <td class="str">{{ $row->address }}</td>
            <td class="str">{{ $row->phone_1 }}</td>
            <td class="str">{{ $row->phone_2 }}</td>
            <td class="str">{{ Main::patient_status_raw($row->status) }}</td>
            <td class="str">{{ Main::format_datetime($row->created_at) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
