<div class="modal" id="modal-payment-detail" role="dialog"
     aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content detail-info">
            <div class="modal-body">
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">No Invoice</label>
                    <div class="col-lg-8">
                        {{ date('Ymd-1') }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Tanggal Invoice</label>
                    <div class="col-lg-8">
                        {{ date('l, d F Y') }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Nama Pasien</label>
                    <div class="col-lg-8">
                        Putra Waryanda
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Umur</label>
                    <div class="col-lg-8">
                        22 Tahun
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Alamat</label>
                    <div class="col-lg-8">
                        Jln. Gadung 1 No 3, Desa Sibangkaja, Abiansemal, Badung
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Berat Badan</label>
                    <div class="col-lg-2">
                        77 KG
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">No HP/WhatsApp</label>
                    <div class="col-lg-8">
                        081 934 364 063
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">No HP Lainnya</label>
                    <div class="col-lg-8">
                        081 934 364 063
                    </div>
                </div>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="10">No</th>
                        <th>Deskripsi</th>
                        <th>Qty</th>
                        <th>Harga</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="number">1.</td>
                        <td>Fibumin</td>
                        <td class="number">2</td>
                        <td class="number">235,000</td>
                        <td class="number">470,000</td>
                    </tr>
                    <tr>
                        <td class="number">2.</td>
                        <td>Fituno</td>
                        <td class="number">1</td>
                        <td class="number">100,000</td>
                        <td class="number">100,000</td>
                    </tr>
                    </tbody>
                </table>

                <div class="row">
                    <div class="offset-lg-8 col-lg-4">
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td class="bold">Total</td>
                                <td class="number" align="right">570,000</td>
                            </tr>
                            <tr>
                                <td class="bold">Biaya Tambahan</td>
                                <td class="number" align="right">0</td>
                            </tr>
                            <tr>
                                <td class="bold">Potongan</td>
                                <td class="number" align="right">0</td>
                            </tr>
                            <tr>
                                <td class="bold">Grand Total</td>
                                <td class="number" align="right">570,000</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
