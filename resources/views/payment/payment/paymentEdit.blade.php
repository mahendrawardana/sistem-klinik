@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datetimepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/calendar/basic.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('userList')}}">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>


        <form action="{{ route('paymentUpdate', ['id_payment' => Main::encrypt($payment->id_payment)]) }}"
              method="post"
              class="form-send"
              data-alert-show="true"
              data-alert-field-message="true">

            {{ csrf_field() }}

            <input type="hidden" name="total" value="{{ $payment->total }}">
            <input type="hidden" name="grand_total" value="{{ $payment->grand_total }}">
            <input type="hidden" name="id_action" value="{{ $payment->id_action }}">
            <input type="hidden" name="id_patient" value="{{ $payment->id_patient }}">

            <div class="m-content">
                <div class="m-portlet akses-list">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Payment
                                        <small>
                                            Form untuk item yang perlu dibayar
                                        </small>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">

                            <button type="button"
                                    class="btn btn-success m-btn m-btn--pill m-btn--air btn-payment-add">
                                <i class="la la-plus"></i> Tambah Item Pembayaran
                            </button>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-checkable datatable-general table-payment-list">
                                <thead>
                                <tr>
                                    <th>Deskripsi</th>
                                    <th width="100">Qty</th>
                                    <th width="150">Harga</th>
                                    <th width="150">Total</th>
                                    <th width="60">Menu</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payment->payment_detail as $row)
                                    <tr>
                                        <td>
                                            <textarea class="form-control m-input m-input--pill" name="description[]"
                                                      style="min-width: 200px; max-width: 100%">{{ $row->description }}</textarea>
                                        </td>
                                        <td class="td-payment-qty">
                                            <input type="text"
                                                  class="form-control m-input m-input--pill input-numeral payment-qty"
                                                  name="qty[]" value="{{ $row->qty }}"
                                                  style="min-width: 80px">
                                        </td>
                                        <td align="right" class="td-payment-price">
                                            <input type="text"
                                                   class="form-control m-input m-input--pill input-numeral payment-price"
                                                   name="price[]"
                                                   value="{{ $row->price }}" style="min-width: 140px">
                                        </td>
                                        <td align="right" class="payment-td-sub-total">{{ Main::format_number($row->total) }}</td>
                                        <td>
                                            <button class="btn btn-sm btn-danger m-btn--pill btn-payment-row-remove"
                                                    type="button">
                                                <i class="la la-remove"></i> Hapus
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-lg-5 offset-lg-7">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body row">
                                <div class="col-lg-12">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input"
                                               class="col-lg-4 col-form-label">Total</label>
                                        <div class="col-lg-8 col-form-label">
                                            <strong><span
                                                        class="payment-total">{{ Main::format_number($payment->total) }}</span></strong>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-lg-4 col-form-label">Biaya
                                            Tambahan</label>
                                        <div class="col-lg-8">
                                            <input class="form-control m-input input-numeral payment-plus"
                                                   type="text"
                                                   name="payment_plus" value="{{ $payment->additional_cost }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input"
                                               class="col-lg-4 col-form-label">Potongan</label>
                                        <div class="col-lg-8">
                                            <input class="form-control m-input input-numeral payment-cut"
                                                   type="text"
                                                   name="payment_cut" value="{{ $payment->discount }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-lg-4 col-form-label">Grand
                                            Total</label>
                                        <div class="col-lg-8 col-form-label">
                                            <strong><span class="payment-grand-total"
                                                          style="font-size: 22px">{{ Main::format_number($payment->grand_total) }}</span></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="produksi-buttons ">
                    <button type="submit"
                            class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <i class="la la-check"></i> Simpan
                    </button>

                    <a href="{{ route('paymentList') }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <i class="la la-angle-double-left"></i>
                        Kembali ke Daftar
                    </a>
                </div>

            </div>

        </form>
    </div>

    <table class="table-payment-row hidden">
        <tbody>
        <tr>
            <td>
                <textarea class="form-control m-input m-input--pill" name="description[]"
                          style="min-width: 200px; max-width: 100%"></textarea>
            </td>
            <td class="td-payment-qty"><input type="text"
                                              class="form-control m-input m-input--pill input-numeral payment-qty"
                                              name="qty[]" value="1" style="min-width: 80px"></td>
            <td align="right" class="td-payment-price">
                <input type="text" class="form-control m-input m-input--pill input-numeral payment-price" name="price[]"
                       value="0" style="min-width: 140px">
            </td>
            <td align="right" class="payment-td-sub-total">0</td>
            <td>
                <button class="btn btn-sm btn-danger m-btn--pill btn-payment-row-remove" type="button">
                    <i class="la la-remove"></i> Hapus
                </button>
            </td>
        </tr>
        </tbody>
    </table>

@endsection
