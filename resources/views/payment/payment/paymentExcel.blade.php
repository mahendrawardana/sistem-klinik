@php

    header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
    header("Content-Disposition: attachment; filename=data_pembayaran_dari_".$date_from."_sampai_".$date_to.".xls");  //File name extension was wrong
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false);

@endphp

<style> .str{ mso-number-format:\@; } </style>

<table width="100%" border="1">
    <caption>{{ 'Data Pembayaran '.Main::format_date_label($date_from).' - '.Main::format_date_label($date_to) }}</caption>
    <thead>
    <tr>
        <th width="20">No</th>
        <th>Nama</th>
        <th>Tanggal Lahir</th>
        <th>Umur</th>
        <th>Berat Badan</th>
        <th>Provinsi</th>
        <th>Kabupaten</th>
        <th>Kecamatan</th>
        <th>Alamat</th>
        <th>Telepon 1</th>
        <th>Telepon 2</th>
        <th>Status</th>
        <th>Tanggal Input Data</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data_list as $no => $row)
        <tr>
            <td align="center" class="str" width="50">{{ ++$no }}.</td>
            <td class="str">{{ $row->patient->name }}</td>
            <td class="str">{{ Main::format_date_label($row->patient->birthday) }}</td>
            <td class="str">{{ Main::format_age($row->patient->birthday) }}</td>
            <td class="str">{{ $row->patient->weight }} Kg.</td>
            <td class="str">{{ $row->patient->province->province_name }}</td>
            <td class="str">{{ $row->patient->city->city_name }}</td>
            <td class="str">{{ $row->patient->subdistrict->subdistrict_name }}</td>
            <td class="str">{{ $row->patient->address }}</td>
            <td class="str">{{ $row->patient->phone_1 }}</td>
            <td class="str">{{ $row->patient->phone_2 }}</td>
            <td class="str">{{ Main::patient_status_raw($row->patient->status) }}</td>
            <td class="str">{{ Main::format_datetime($row->patient->created_at) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
