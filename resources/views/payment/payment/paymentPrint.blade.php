<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <style type="text/css">
        body {
            font-size: 11px;
        }

        h1 {
            font-size: 20px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h2 {
            font-size: 16px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h3 {
            font-size: 12px;
            font-weight: bold;
        }

        .div-2 {
            width: 50%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 {
            width: 33.3%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 tr td {
            vertical-align: top;
            padding-top: 0;
        }

        .label-1 {
            margin-top: -10px;
            margin-bottom: -14px;
            padding-left: 30%;
        }

        .label-1 label {
            padding-top: 12px;
        }

        .label-2 {
            margin-top: -5px;
            margin-bottom: -6px;
            padding-left: 30%;
        }

        .label-2 label {
            padding-top: 12px;
        }

        .table th, .table td {
            padding: 2px 4px;
        }

        .table th {
            text-align: center;
        }

        .table th.number, .table td.number {
            text-align: right;
        }

        .bold {
            font-weight: bold;
        }


    </style>
    <?php
    $width_label = '64';
    ?>

</head>
<body>
<div class="text-center">
    <img src="{{ asset('images/rumah-sunat-bali-logo.png') }}">
</div>
<h1 align="center">KWITANSI PEMBAYARAN</h1>
<hr/>

Invoice {{ $payment->invoice_label }}<br/>
Invoice date: {{ date('l, d F Y', strtotime($payment->invoice_date)) }}<br/>
Jl. Tukad Batang Hari No. 42, Panjer - Denpasar Selatan, Denpasar - Bali<br/>
(0361) 4783083 / 0877-7711-4800<br/><br/><br/>

<h3>Atas Nama : {{ $payment->patient->name }}</h3>
<h3>Alamat
    : {{ $payment->patient->province->province_name.', '.$payment->patient->city->city_name.', '.$payment->patient->subdistrict->subdistrict_name.', '. $payment->patient->address }}</h3>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="10">No</th>
        <th>Deskripsi</th>
        <th>Qty</th>
        <th>Harga</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($payment->payment_detail as $key => $row)
        <tr>
            <td class="number">{{ $key + 1 }}.</td>
            <td>{{ $row->description }}</td>
            <td class="number">{{ \app\Helpers\Main::format_number($row->qty) }}</td>
            <td class="number">{{ \app\Helpers\Main::format_number($row->price) }}</td>
            <td class="number">{{ \app\Helpers\Main::format_number($row->total) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<div class="div-3"></div>
<div class="div-3"></div>
<div class="div-3">
    <table width="100%">
        <tbody>
        <tr>
            <td class="bold">Total</td>
            <td class="number" align="right">{{ \app\Helpers\Main::format_number($payment->total) }}</td>
        </tr>
        <tr>
            <td class="bold">Biaya Tambahan</td>
            <td class="number" align="right">{{ \app\Helpers\Main::format_number($payment->additional_cost) }}</td>
        </tr>
        <tr>
            <td class="bold">Potongan</td>
            <td class="number" align="right">{{ \app\Helpers\Main::format_number($payment->discount) }}</td>
        </tr>
        <tr>
            <td class="bold">Grand Total</td>
            <td class="number" align="right">{{ \app\Helpers\Main::format_number($payment->grand_total) }}</td>
        </tr>
        </tbody>
    </table>
</div>
<div class="clearfix"></div>
<br/>
<br/>
<br/>
<div class="div-2">
    <br />
    <br />
    <br />
    <br />
    <strong>Cahier</strong> : {{ $karyawan->nama_karyawan }}<br/>
    Terimakasih atas kunjungan dan kepercayaan Anda
</div>
<div class="div-2 text-center">
    Panjer, {{ date('d F Y', strtotime($payment->invoice_date)) }}<br />
    <br />
    <br />
    <br />
    <br />
    <strong>{{ $payment->patient->name }}</strong><br/>
</div>
<div class="clearfix"></div>

</body>
