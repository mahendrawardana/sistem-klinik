@extends('../general/index')

@section('css')
    {!! $schedule_calendar_modal['css'] !!}
@endsection

@section('js')
    {!! $schedule_calendar_modal['js'] !!}
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('userList')}}">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet akses-list">
                <div class="m-portlet__body">
                    <div id="m_calendar"></div>
                </div>
            </div>

        </div>

    </div>
@endsection
